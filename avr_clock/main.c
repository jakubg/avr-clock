#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <stdlib.h>
#include <string.h>

#include "lib/lcd.h"
#include "lib/i2c.h"
#include "main.h"


/* 
 * Pomocnicze
*/
uint8_t dec2bcd(uint8_t dec) { // Funckja konwertujaca system 10 na BCD.
	return ((dec / 10)<<4) | (dec % 10);
}

uint8_t bcd2dec(uint8_t bcd) { // Funckja konwertujaca system BCD na 10.
	return ((((bcd) >> 4) & 0x0F) * 10) + ((bcd) & 0x0F);
}

uint8_t leapYear(uint16_t year) { // Funckja sprawdzajaca czy dany rok jest przystepny.
	return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}

uint8_t checkDate(uint16_t year, uint16_t months, uint16_t days) { // Funkcja sprawdzajaca poprawnosc podanej daty.
	uint16_t monthsNonLeap[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	uint16_t monthsLeap[] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	if(leapYear(year)) { if(days > monthsLeap[months-1]) return 1; }
		else if(days > monthsNonLeap[months-1]) return 1;
	return 0;
}


/*
 * RTC
*/

/*
 * Inicjowanie RTC, ustawienie SQW na 1Hz
*/
void DS3231_init(void){
	uint8_t arg = 0;
	TWI_write_buf( DS3231_ADDR, 0x0e, 1, &arg);
}

/*
 * Funkcja pobierajaca czas i date z modulu i2c.
*/
void DS3231_get_datetime( TDATETIME * dt ) {
	uint8_t i;
	uint8_t buf[7];
	TWI_read_buf( DS3231_ADDR, 0x00, 7, buf );
	for(i=0; i<7; i++) dt->bytes[i] = bcd2dec(buf[i]);
}

/*
 * Funkcja wyswietlajaca czas.
*/
void show_time( TDATETIME * dt ) {
	if( dt->hh < 10 ) LCD_WriteText("0");
	LCD_WriteInt(dt->hh);
	LCD_WriteText(":");
	if( dt->mm < 10 ) LCD_WriteText("0");
	LCD_WriteInt(dt->mm);
	LCD_WriteText(":");
	if( dt->ss < 10 ) LCD_WriteText("0");
	LCD_WriteInt(dt->ss);
}

/*
 * Funkcja wyswietlajaca date.
*/
void show_date( TDATETIME * dt ) {
	LCD_WriteText("20");
	if( dt->year < 10 ) LCD_WriteText("0");
	LCD_WriteInt(dt->year);
	LCD_WriteText("-");
	if( dt->month < 10 ) LCD_WriteText("0");
	LCD_WriteInt(dt->month);
	LCD_WriteText("-");
	if( dt->day < 10 ) LCD_WriteText("0");
	LCD_WriteInt(dt->day);
	LCD_WriteText(", ");
	LCD_WriteText( days[ dt->dayofwek - 1 ] );
}

/*
 * Funkcja pobierajaca temperature po protokole i2c.
*/
void DS3231_get_temp( TTEMP * tmp ) {
	uint8_t buf[2];
	TWI_read_buf( DS3231_ADDR, 0x11, 2, buf );
	tmp->cel = buf[0] ;
	tmp->fract = buf[1]>>6;

	TWI_read_buf( DS3231_ADDR, 0x0e, 1, buf );
	buf[0] |= (1<<5);
	TWI_write_buf( DS3231_ADDR, 0x0e, 1, buf );
}

/*
 * Funkcja wyswietlajaca temperature w formie XX.X'C.
*/
void show_temperature( TTEMP * tmp ) {
	if(tmp->fract > 0 && tmp->fract < 5) tmp->fract = 5;
	if(tmp->fract > 5) { tmp->fract = 0; tmp->cel += 1; }
	if( tmp->cel < 10 ) LCD_WriteText("0");
	LCD_WriteInt(tmp->cel);
	LCD_WriteText(".");
	LCD_WriteInt(tmp->fract);
	LCD_WriteText("'C");
}

/*
 * Funkcja ustawiajaca date, pobiera parametry YY.MM.DD, Dow.
*/
void DS3231_set_date( uint8_t year, uint8_t month, uint8_t day, uint8_t dayofweek ) {
	uint8_t buf[4];
	buf[0]=dayofweek;
	buf[1]=dec2bcd(day);
	buf[2]=dec2bcd(month);
	buf[3]=dec2bcd(year);
	TWI_write_buf( DS3231_ADDR, 0x03, 4, buf );
}

/*
 * Funkcja ustawiajaca godzine, pobiera parametry HH:MM:SS
*/
void DS3231_set_time( uint8_t hh, uint8_t mm, uint8_t ss ) {
	uint8_t buf[3];
	buf[0]=dec2bcd(ss);
	buf[1]=dec2bcd(mm);
	buf[2]=dec2bcd(hh);
	TWI_write_buf( DS3231_ADDR, 0x00, 3, buf );
}


/*
 * BUTTONS, inicializacja przyciskow pod odpowiednimi portami.
*/
void init_buttons(void){
	DDRD &= ~K1;
	PORTD |= K1;
	PCMSK2 |= (1<<PCINT23);
	PCICR |= (1<<PCIE2);

	DDRB &= ~K2;
	PORTB |= K2;
	PCMSK0 |= (1<<PCINT0);
	PCICR |= (1<<PCIE0);
}

/*
 * Funkcja ustawiajaca wartosc zmiennych, podczas zmiany godziny, daty.
*/
void setValue(uint8_t mswitch) {
	switch(mswitch) {
		case 1: {
			set_hh++;
			if(set_hh > 23) set_hh = 0;
			LCD_GoTo(4,1);
			if(set_hh < 10) LCD_WriteInt(0);
			LCD_WriteInt(set_hh);
			break;
		}
		case 2: {
			set_mm++;
			if(set_mm > 59) set_mm = 0;
			LCD_GoTo(7,1);
			if(set_mm < 10) LCD_WriteInt(0);
			LCD_WriteInt(set_mm);
			break;
		}
		case 3: {
			set_ss++;
			if(set_ss > 59) set_ss = 0;
			LCD_GoTo(10,1);
			if(set_ss < 10) LCD_WriteInt(0);
			LCD_WriteInt(set_ss);
			break;
		}
		case 4: {
			break;
		}
		case 5: {
			set_year++;
			if(set_year > 99) set_year = 10;
			LCD_GoTo(5,1);
			LCD_WriteInt(set_year);
			break;
		}
		case 6: {
			set_month++;
			if(set_month > 12) set_month = 1;
			LCD_GoTo(8,1);
			if(set_month < 10) LCD_WriteInt(0);
			LCD_WriteInt(set_month);
			break;
		}
		case 7: {
			set_day++;
			if(set_day > 31) set_day = 1;
			LCD_GoTo(11,1);
			if(set_day < 10) LCD_WriteInt(0);
			LCD_WriteInt(set_day);
			break;
		}
		case 8: {
			set_dayofweek++;
			if(set_dayofweek > 7) set_dayofweek = 1;
			LCD_GoTo(11,2);
			showDayOfWeek(set_dayofweek);
			break;
		}
		case 9: {
			break;
		}
		case 10: {
			break;
		}
	}
}

/*
 * Funkcja pomocnicza, wyswietlajaca odpowiedni dzien tygodnia.
*/
void showDayOfWeek(int16_t dow) {
	switch(dow) {
		case 1: LCD_WriteText("Pn"); break;
		case 2: LCD_WriteText("Wt"); break;
		case 3: LCD_WriteText("Sr"); break;
		case 4: LCD_WriteText("Cz"); break;
		case 5: LCD_WriteText("Pt"); break;
		case 6: LCD_WriteText("So"); break;
		case 7: LCD_WriteText(" N"); break;
	}
}

/*
 * Funkcja wyswietlajaca menu do ustawien daty, godziny.
*/
void showMenu(uint8_t mswitch) {
	switch(mswitch) {
		case 0: {
			EIMSK &= ~(1<<INT0);
			mainswitch = 1;
			LCD_Clear();
			LCD_Home();
			LCD_WriteText("|  Ustaw czas  |");
			LCD_GoTo(0,1);
			LCD_WriteText("|   00:__:__   |");
			LCD_GoTo(0,2);
			LCD_WriteText("|              |");
			LCD_GoTo(0,3);
			LCD_WriteText("|______________|");
			break;
		}
		case 1: {
			mainswitch = 2;
			LCD_Clear();
			LCD_Home();
			LCD_WriteText("|  Ustaw czas  |");
			LCD_GoTo(0,1);
			LCD_WriteText("|   ");
			if(set_hh < 10) LCD_WriteInt(0);
			LCD_WriteInt(set_hh);
			LCD_WriteText(":00:__   |");
			LCD_GoTo(0,2);
			LCD_WriteText("|              |");
			LCD_GoTo(0,3);
			LCD_WriteText("|______________|");
			break;
		}
		case 2: {
			mainswitch = 3;
			LCD_Clear();
			LCD_Home();
			LCD_WriteText("|  Ustaw czas  |");
			LCD_GoTo(0,1);
			LCD_WriteText("|   ");
			if(set_hh < 10) LCD_WriteInt(0);
			LCD_WriteInt(set_hh);
			LCD_WriteText(":");
			if(set_mm < 10) LCD_WriteInt(0);
			LCD_WriteInt(set_mm);
			LCD_WriteText(":00   |");
			LCD_GoTo(0,2);
			LCD_WriteText("|              |");
			LCD_GoTo(0,3);
			LCD_WriteText("|______________|");
			break;
		}
		case 3: {
			DS3231_set_time(set_hh,set_mm,set_ss);
			mainswitch = 4;
			LCD_Clear();
			LCD_Home();
			LCD_WriteText("|  *Ustawiono  |");
			LCD_GoTo(0,1);
			LCD_WriteText("|  godzine na  |");
			LCD_GoTo(0,2);
			LCD_WriteText("|   ");
			if(set_hh < 10) LCD_WriteInt(0);
			LCD_WriteInt(set_hh);
			LCD_WriteText(":");
			if(set_mm < 10) LCD_WriteInt(0);
			LCD_WriteInt(set_mm);
			LCD_WriteText(":");	
			if(set_ss < 10) LCD_WriteInt(0);		
			LCD_WriteInt(set_ss);
			LCD_WriteText("   |");
			LCD_GoTo(0,3);
			LCD_WriteText("|______________|");
			set_hh = 0, set_mm = 0, set_ss = 0;
			break;
		}
		case 4: {
			mainswitch = 5;
			LCD_Clear();
			LCD_Home();
			LCD_WriteText("|  Ustaw date  |");
			LCD_GoTo(0,1);
			LCD_WriteText("|  2010.__.__  |");
			LCD_GoTo(0,2);
			LCD_WriteText("|         [  ] |");
			LCD_GoTo(0,3);
			LCD_WriteText("|______________|");
			break;
		}
		case 5: {
			mainswitch = 6;
			LCD_Clear();
			LCD_Home();
			LCD_WriteText("|  Ustaw date  |");
			LCD_GoTo(0,1);
			LCD_WriteText("|  20");
			LCD_WriteInt(set_year);
			LCD_WriteText(".01.__  |");
			LCD_GoTo(0,2);
			LCD_WriteText("|         [  ] |");
			LCD_GoTo(0,3);
			LCD_WriteText("|______________|");
			break;
		}
		case 6: {
			mainswitch = 7;
			LCD_Clear();
			LCD_Home();
			LCD_WriteText("|  Ustaw date  |");
			LCD_GoTo(0,1);
			LCD_WriteText("|  20");
			LCD_WriteInt(set_year);
			LCD_WriteText(".");
			if(set_month < 10) LCD_WriteInt(0);
			LCD_WriteInt(set_month);
			LCD_WriteText(".01  |");
			LCD_GoTo(0,2);
			LCD_WriteText("|         [  ] |");
			LCD_GoTo(0,3);
			LCD_WriteText("|______________|");
			break;
		}
		case 7: {
			mainswitch = 8;
			LCD_Clear();
			LCD_Home();
			LCD_WriteText("|  Ustaw date  |");
			LCD_GoTo(0,1);
			LCD_WriteText("|  20");
			LCD_WriteInt(set_year);
			LCD_WriteText(".");
			if(set_month < 10) LCD_WriteInt(0);
			LCD_WriteInt(set_month);
			LCD_WriteText(".");
			if(set_day < 10) LCD_WriteInt(0);
			LCD_WriteInt(set_day);
			LCD_WriteText("  |");
			LCD_GoTo(0,2);
			LCD_WriteText("|         [Pn] |");
			LCD_GoTo(0,3);
			LCD_WriteText("|______________|");
			break;
		}
		case 8: {
			if(checkDate(set_year,set_month,set_day) == 0) {
				DS3231_set_date(set_year,set_month,set_day,set_dayofweek);
				mainswitch = 9;
				LCD_Clear();
				LCD_Home();
				LCD_WriteText("|  *Ustawiono  |");
				LCD_GoTo(0,1);
				LCD_WriteText("|   date na    |");
				LCD_GoTo(0,2);
				LCD_WriteText("|  20");
				LCD_WriteInt(set_year);
				LCD_WriteText(".");
				if(set_month < 10) LCD_WriteInt(0);
				LCD_WriteInt(set_month);
				LCD_WriteText(".");	
				if(set_day < 10) LCD_WriteInt(0);
				LCD_WriteInt(set_day);
				LCD_WriteText("  |");
				LCD_GoTo(0,3);
				LCD_WriteText("|_________[");
				showDayOfWeek(set_dayofweek);
				LCD_WriteText("]_|");
				set_year = 10, set_month = 1, set_day = 1, set_dayofweek = 1;
				break;
			} else {
				mainswitch = 4;
				set_year = 10, set_month = 1, set_day = 1, set_dayofweek = 1;
				LCD_GoTo(0,0);
				LCD_WriteText(" ______________ ");
				LCD_GoTo(0,1);
				LCD_WriteText("| Kernel panic |");
				LCD_GoTo(0,2);
				LCD_WriteText("| Popraw date! |");
				LCD_GoTo(0,3);
				LCD_WriteText("|______________|");
				break;
			}
		}
		case 9: {
			LCD_Clear();
			EIMSK |= (1<<INT0);
			mainswitch = 0;
			break;
		}
	}
}

/* 
 * MAIN
*/

int main(void){
	LCD_Initialize();
	LCD_Clear();
	LCD_Home();
	
	i2cSetBitrate(100); //Ustawiamy predkosc magistrali i2c.
	init_buttons();
	
	DS3231_init();

	/*
	 * Uruchamiamy przerwania zewnetrzne rising, na porcie INT0.
	*/
	//EICRA = (EICRA & ~((1<<ISC01)|(1<<ISC00)));
	//EIMSK |= (1<<INT0);
	EICRA = 0x03;
	EIMSK = 0x01;

	/*
	 * Globalnie uruchamiamy przerwania, oraz wchodzimy w nieskonczona petle.
	*/
	sei();
	while(1);
}

/*
 * Obsluga przerwania zenwetrznego SQW.
*/
SIGNAL(INT0_vect) {
	if(mainswitch == 0) {
		DS3231_get_datetime( &datetime );
		DS3231_get_temp( &temperature );
		LCD_GoTo(0, 0);
		LCD_WriteText(" ______________ ");
		LCD_GoTo(0, 1);
		LCD_WriteText("|   ");
		LCD_GoTo(4, 1);
		show_time(&datetime);
		LCD_WriteText("   |");
		LCD_GoTo(0, 2);
		LCD_WriteText("|");
		show_date(&datetime);
		LCD_WriteText("|");
		LCD_GoTo(0, 3);
		LCD_WriteText(" --- ");
		LCD_GoTo(5, 3);
		show_temperature(&temperature);
		LCD_WriteText(" ---");
	}
}

/*
 * Funkcja obslugujaca przerwanie lewego przycisku MENU.
*/
ISR(PCINT2_vect) {
	_delay_ms(70);
 	if(K1_DOWN) {
		showMenu(mainswitch);
	}
}

/*
 * Funkcja obslugujaca przerwanie prawego przycisku SET.
*/
ISR(PCINT0_vect) {
	_delay_ms(70);
 	if(K2_DOWN) {
		setValue(mainswitch);
	}
}