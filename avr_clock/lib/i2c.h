/*
 * Plik konfiguracyjny protokolu i2c.
*/
#ifndef I2C_H_
#define I2C_H_

/*
 * Stale, definicje oraz adres pod ktorym znajduje sie RTC.
*/

#define ACK				1
#define NACK			0

/*
 * Adresy modulow i2c.
*/
#define DS3231_ADDR		0xD0		// adres pod ktorym znajduje sie RTC na magistrali i2c.
#define BH170_ADDR		0x5C		// -||- luxometr.

/*
 * Definicje funkcji.
*/

void i2cSetBitrate(uint16_t bitrateKHz);
void TWI_start(void);
void TWI_stop(void);
void TWI_write(uint8_t bajt);
uint8_t TWI_read(uint8_t ack);
void TWI_write_buf( uint8_t SLA, uint8_t adr, uint8_t len, uint8_t *buf );
void TWI_read_buf(uint8_t SLA, uint8_t adr, uint8_t len, uint8_t *buf);

#endif /* I2C_H_ */
