/*
 * Plik konfiguracyjny wyswietlacza LCD HD44780.
*/
#ifndef LCD_H_
#define LCD_H_

/*
 * Definicje portow i kierunkow dla LCD.
*/
#define LCD_RS_DIR		DDRB			//Ustalenia kierunku
#define LCD_E_DIR		DDRB
#define LCD_P4_DIR		DDRD
#define LCD_P5_DIR		DDRD
#define LCD_P6_DIR		DDRD
#define LCD_P7_DIR		DDRD

#define LCD_RS_PORT		PORTB
#define LCD_E_PORT		PORTB
#define LCD_P4_PORT		PORTD
#define LCD_P5_PORT		PORTD
#define LCD_P6_PORT		PORTD
#define LCD_P7_PORT		PORTD

#define LCD_RS			(1<<PORTB4)		//Definicje portow
#define LCD_E			(1<<PORTB3)
#define LCD_P4			(1<<PORTD6)
#define LCD_P5			(1<<PORTD5)
#define LCD_P6			(1<<PORTD4)
#define LCD_P7			(1<<PORTD3)
/*
 * Sta�e wyswietlacza.
*/ 
#define LCD_CLEAR					0x01
#define LCD_HOME					0x02
#define LCD_ENTRY_MODE				0x04
#define LCD_EM_SHIFT_CURSOR			0x00
#define LCD_EM_INCREMENT			0x02
#define LCD_DISPLAY_ONOFF			0x08
#define LCD_DISPLAY_OFF				0x00
#define LCD_DISPLAY_ON				0x04
#define LCD_CURSOR_OFF				0x00
#define LCD_FUNCTION_SET			0x20
#define LCD_FONT5x7					0x00
#define LCD_TWO_LINE				0x08
#define LCD_4_BIT					0x00
#define LCD_DDRAM_SET				0x80

/*
 * Makrodefinicje dla LCD.
*/ 
#define SET_RS			LCD_RS_PORT |= LCD_RS
#define CLR_RS			LCD_RS_PORT &= ~LCD_RS
#define SET_E			LCD_E_PORT |= LCD_E
#define CLR_E			LCD_E_PORT &= ~LCD_E

/*
 * Definicje funkcji.
*/
void LCD_Initialize(void);
void LCD_WriteCommand(unsigned char);
void LCD_WriteData(unsigned char);
void LCD_WriteText(char *);
void LCD_WriteInt(int);
void LCD_GoTo(unsigned char x, unsigned char y);
void LCD_Clear(void);
void LCD_Home(void);
void LCD_DefChar(uint8_t nr, uint8_t *def_znak);
void _LCD_sendHalf(unsigned char data);

#endif /* LCD_H_ */
