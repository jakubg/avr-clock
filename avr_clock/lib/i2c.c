/* 
 * Implementacja obsługi interfejsu I2C (sprzętowa).
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "i2c.h"

/*
 * Funkcja licząca taktowanie szyny i2c na podstawie taktowania mikroprocesora.
 * Wartość jest ustawiana w rejestrze TWBR.
*/
void i2cSetBitrate(uint16_t bitrateKHz) {
	uint8_t bitrate_div;

	bitrate_div = ((F_CPU/1000l)/bitrateKHz);
	if(bitrate_div >= 16) bitrate_div = (bitrate_div-16)/2;
	TWBR = bitrate_div;
}

/*
 * Wykonanie sekwencji startu.
 * TWINT zerujemy flage, odblokowywując jednocześnie przerwanie TWI.
 * TWEN odblokowanie interfejsu TWI.
 * TWSTA generowanie w trybie Master sekwencji trybu startu.
*/
void TWI_start(void) {
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTA);
	while (!(TWCR&(1<<TWINT)));
}

/*
 * Funkcja STOPU.
 * Analogicznie to samo, jedyna zmiana jest ustawienie bitu TWSTO (stopu).
*/
void TWI_stop(void) {
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTO);
	while ( (TWCR&(1<<TWSTO)) );
}

/*
 * Funckja nadawcza.
 * Do rejestru nadawczego TWDR wysylamy bajt do nadania, nastepnie odblokowywujemy przerwanie TWI. W petli oczekujemy na przeslanie.
*/
void TWI_write(uint8_t bajt) {
	TWDR = bajt;
	TWCR = (1<<TWINT)|(1<<TWEN);
	while ( !(TWCR&(1<<TWINT)));
}

/*
 * Funkcja czytajaca bajt z magistrali TWI, rozpoczynajac odbieranie wiekszej ilosci bajtow niz jeden ustawiamy flage ACK na 1.
*/
uint8_t TWI_read(uint8_t ack) {
	TWCR = (1<<TWINT)|(ack<<TWEA)|(1<<TWEN);
	while ( !(TWCR & (1<<TWINT)));
	return TWDR;
}

/*
 * Funkcja ktore uzywa funkcji podstawowych, umozliwia zapisywanie wiekszej ilosci bajtow danych do magistrali i2c.
 * Parametry tej funkcji to: Adres modulu i2c, adres od ktorego rozpoczynamy zapisywanie danych, ilosc danych do zapisu, bufor do odczytu.
*/
void TWI_write_buf( uint8_t SLA, uint8_t adr, uint8_t len, uint8_t *buf ) {
	TWI_start();
	TWI_write(SLA);
	TWI_write(adr);
	while (len--) TWI_write(*buf++);
	TWI_stop();
}

/*
 * Funkcja ktore uzywa funkcji podstawowych, umozliwia odczytywanie wiekszej ilosci bajtow danych z magistrali i2c.
 * Parametry tej funkcji to: Adres modulu i2c, adres od ktorego rozpoczynamy odczytywanie danych, ilosc danych do odebrania, bufor do zapisu.
*/
void TWI_read_buf(uint8_t SLA, uint8_t adr, uint8_t len, uint8_t *buf) {
	TWI_start();
	TWI_write(SLA);
	TWI_write(adr);
	TWI_start();
	TWI_write(SLA + 1);
	while (len--) *buf++ = TWI_read( len ? ACK : NACK );
	TWI_stop();
}
