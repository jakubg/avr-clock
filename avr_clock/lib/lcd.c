/* 
 * LCD HD44780 (Dla 16x4), bez obslugi odczytu.
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <util/delay.h>
#include "lcd.h"


/*
 * Wysy�anie po��wek (na odpowiednie piny).
*/

inline void _LCD_sendHalf(unsigned char data) {
	if(data & 0x01) LCD_P4_PORT |= LCD_P4;
		else LCD_P4_PORT  &= ~LCD_P4;
	if(data & 0x02) LCD_P5_PORT |= LCD_P5;
		else LCD_P5_PORT  &= ~LCD_P5;
	if(data & 0x04) LCD_P6_PORT |= LCD_P6;
		else LCD_P6_PORT  &= ~LCD_P6;
	if(data & 0x08) LCD_P7_PORT |= LCD_P7;
		else LCD_P7_PORT  &= ~LCD_P7;
}

/*
 * Funckja do wysylania calego bajtu.
*/
void _LCD_Write(unsigned char dataToWrite) {
	SET_E;
	_LCD_sendHalf(dataToWrite >> 4); //starsza czesc
	CLR_E;
	SET_E;
	_LCD_sendHalf(dataToWrite); //mlodsza
	CLR_E;
	_delay_us(60);
}

/*
 * Okreslajac stan lini RS, przesylamy komende do wyswietlacza lCD.
*/
void LCD_WriteCommand(unsigned char commandToWrite) {
	CLR_RS;
	_LCD_Write(commandToWrite);
}

/*
 * Okreslajac stan lini RS, przesylamy kod znaku do wyswietlacza lCD.
*/
void LCD_WriteData(unsigned char dataToWrite) {
	SET_RS;
	_LCD_Write(dataToWrite);
}

/*
 * Funckja wysylajaca podane dane jako znaki do wyswietlacza.
*/
void LCD_WriteText(char *text) {
	while(*text) LCD_WriteData(*text++);
}

/*
 * Funckja przyjmujaca za parametr typ integer, uzywa funkcji itoa i nastepnie wysyla dane do ww funkcji.
*/
void LCD_WriteInt(int val) {
	char bufor[17];
	LCD_WriteText( itoa(val, bufor, 10) );
}

/*
 * Funcja skaczaca do odpowiedniej pozycji na wyswietlaczu, przeliczenia sa dokonywane dla wyswietlacza 4x16.
*/
void LCD_GoTo(unsigned char x, unsigned char y) {
	switch(y) {
		case 0: y = 0x00; break; // adres 1 znaku 1 wiersza
		case 1: y = 0x40; break; // adres 1 znaku 2 wiersza
		case 2: y = 0x10; break; // adres 1 znaku 3 wiersza
		case 3: y = 0x50; break; // adres 1 znaku 4 wiersza
	}
	LCD_WriteCommand( (0x80 + y + x) );
}

/*
 * Funkcja typu alias, czyszczaca ekran.
*/
void LCD_Clear(void) {
	LCD_WriteCommand(LCD_CLEAR);
	_delay_ms(2);
}

/*
 * Funkcja typu alias, skaczaca do pozycji 0,0 na ekranie.
*/
void LCD_Home(void) {
	LCD_WriteCommand(LCD_HOME);
	_delay_ms(2);
}

/*
 * Funkcja iniciujaca wyswietlacz.
 * Przygotowanie portow, wyslanie komend startowych i wstepna konfiguracja.
*/
void LCD_Initialize(void) {
	LCD_P4_DIR |= LCD_P4;
	LCD_P5_DIR |= LCD_P5;
	LCD_P6_DIR |= LCD_P6;
	LCD_P7_DIR |= LCD_P7;
	LCD_E_DIR  |= LCD_E; 
	LCD_RS_DIR |= LCD_RS;

	_delay_ms(15);
	CLR_E;
	CLR_RS;

	SET_E;
	_LCD_sendHalf(0x03);
	CLR_E;
	_delay_ms(5);

	SET_E;
	_LCD_sendHalf(0x03);
	CLR_E;
	_delay_us(100);

	SET_E;
	_LCD_sendHalf(0x03);
	CLR_E;
	_delay_us(100);

	SET_E;
	_LCD_sendHalf(0x02);
	CLR_E;
	_delay_us(100);
	
	LCD_WriteCommand(LCD_FUNCTION_SET | LCD_FONT5x7 | LCD_TWO_LINE | LCD_4_BIT); // znaki 5x7, dwie linie, 4bitowy
	LCD_WriteCommand(LCD_DISPLAY_ONOFF | LCD_CURSOR_OFF);
	LCD_WriteCommand(LCD_ENTRY_MODE | LCD_EM_SHIFT_CURSOR | LCD_EM_INCREMENT); // inkrementaja adresu i przesuwanie kursora
	LCD_WriteCommand(LCD_DISPLAY_ONOFF | LCD_DISPLAY_ON); // wlacz LCD, bez kursora
	LCD_WriteCommand(LCD_CLEAR); // czyscimy DDRAM
	_delay_us(200); 
}
