/*
 * Plik konfiguracyjny.
*/
#ifndef MAIN_H_
#define MAIN_H_

/*
 * Definicje przyciskow.
*/
#define K1 (1<<PORTD7)
#define K1_DOWN !(PIND & K1)
#define K1_UP (PIND & K1)
#define K2 (1<<PORTB0)
#define K2_DOWN !(PINB & K2)
#define K2_UP (PINB & K2)

/*
 * Definicje wartosci wolnych od optymalizacji.
*/
volatile unsigned int mainswitch = 0;

/*
 * Definicje uni, struktur oraz zmiennych na potrzeby zegara.
*/
typedef union {
	uint8_t bytes[7];
	struct {
		uint8_t ss;
		uint8_t mm;
		uint8_t hh;
		uint8_t dayofwek;
		uint8_t day;
		uint8_t month;
		uint8_t year;
	};
} TDATETIME;


typedef struct {
	int8_t cel;
	uint8_t fract;
} TTEMP;

char days[7][4] = {
		"Pn", "Wt", "Sr", "Cz", "Pt", "So", "N"
};

uint16_t set_hh = 0, set_mm = 0, set_ss = 0;
uint16_t set_year = 10, set_month = 1, set_day = 1, set_dayofweek = 1;

TDATETIME datetime;
TTEMP temperature;

/*
 * Definicje funkcji pomocniczych.
*/
uint8_t dec2bcd(uint8_t dec);
uint8_t bcd2dec(uint8_t bcd);
void showDayOfWeek(int16_t dow);
uint8_t leapYear(uint16_t year);
uint8_t checkDate(uint16_t year, uint16_t months, uint16_t days);

void DS3231_init(void);
void show_time( TDATETIME * dt );
void show_date( TDATETIME * dt );
void DS3231_get_temp( TTEMP * tmp );
void show_temperature( TTEMP * tmp );
void DS3231_set_date( uint8_t year, uint8_t month, uint8_t day, uint8_t dayofweek );
void DS3231_set_time( uint8_t hh, uint8_t mm, uint8_t ss );

void setValue(uint8_t mswitch);
void showMenu(uint8_t mswitch);

#endif /* MAIN_H_ */
