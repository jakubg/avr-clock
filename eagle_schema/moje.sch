<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="13" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="15" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="15" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="Ð­">
<libraries>
<library name="MySMD">
<packages>
<package name="QFP032">
<wire x1="-1.2192" y1="0" x2="-1.2192" y2="8.001" width="0.127" layer="51"/>
<wire x1="-1.2192" y1="8.001" x2="6.8326" y2="8.001" width="0.127" layer="51"/>
<wire x1="6.8326" y1="8.001" x2="6.8326" y2="0" width="0.127" layer="51"/>
<wire x1="6.8326" y1="0" x2="-1.2192" y2="0" width="0.127" layer="51"/>
<circle x="-1.27" y="-0.127" radius="0.4016" width="0.2032" layer="21"/>
<smd name="15" x="6.8072" y="5.9944" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="14" x="6.8072" y="5.1816" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="11" x="6.8072" y="2.794" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="9" x="6.8072" y="1.1938" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="8" x="5.5977" y="0" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="7" x="4.7981" y="0" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="6" x="3.9985" y="0" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="5" x="3.1986" y="0" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="4" x="2.399" y="0" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="2" x="0.7996" y="0" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="3" x="1.5994" y="0" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="1" x="0" y="0" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="18" x="4.7981" y="7.9967" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="19" x="3.9985" y="7.9967" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="5.588" y="8.001" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="13" x="6.8072" y="4.3942" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="10" x="6.8072" y="2.0066" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="12" x="6.8072" y="3.6068" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="16" x="6.8156" y="6.8029" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="21" x="2.399" y="7.9967" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="22" x="1.5994" y="7.9967" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="23" x="0.7988" y="7.9967" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="20" x="3.1986" y="7.9967" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="24" x="0" y="7.9967" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="25" x="-1.1938" y="6.8072" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="26" x="-1.1938" y="5.9944" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="27" x="-1.1938" y="5.1816" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="28" x="-1.1938" y="4.3942" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="30" x="-1.1938" y="2.794" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="29" x="-1.1938" y="3.6068" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="32" x="-1.1938" y="1.1938" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="31" x="-1.1938" y="2.0066" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<text x="-0.4572" y="9.3726" size="0.6096" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.8542" size="0.6096" layer="27">&gt;VALUE</text>
</package>
<package name="CAP0805-NP">
<wire x1="0.127" y1="-0.635" x2="0.127" y2="0.635" width="0.127" layer="51"/>
<wire x1="0.127" y1="0.635" x2="2.159" y2="0.635" width="0.127" layer="51"/>
<wire x1="2.159" y1="0.635" x2="2.159" y2="-0.635" width="0.127" layer="51"/>
<wire x1="2.159" y1="-0.635" x2="0.127" y2="-0.635" width="0.127" layer="51"/>
<wire x1="1.143" y1="0.762" x2="1.143" y2="-0.762" width="0.2032" layer="21"/>
<smd name="1" x="0" y="0" dx="1.524" dy="1.524" layer="1"/>
<smd name="2" x="2.286" y="0" dx="1.524" dy="1.524" layer="1"/>
<text x="-0.127" y="1.016" size="0.6096" layer="25">&gt;NAME</text>
<text x="-0.127" y="-1.524" size="0.6096" layer="27">&gt;VALUE</text>
</package>
<package name="TAN-A">
<wire x1="-0.4318" y1="-0.8128" x2="-0.4318" y2="0.8128" width="0.127" layer="51"/>
<wire x1="-0.4318" y1="0.8128" x2="3.2258" y2="0.8128" width="0.127" layer="51"/>
<wire x1="3.2258" y1="0.8128" x2="3.2258" y2="-0.8128" width="0.127" layer="51"/>
<wire x1="3.2258" y1="-0.8128" x2="-0.4318" y2="-0.8128" width="0.127" layer="51"/>
<wire x1="0.8636" y1="0.8128" x2="1.9304" y2="0.8128" width="0.127" layer="21"/>
<wire x1="0.8636" y1="-0.8128" x2="1.9304" y2="-0.8128" width="0.127" layer="21"/>
<wire x1="-0.8128" y1="0.9652" x2="-0.8128" y2="-0.9652" width="0.127" layer="21"/>
<smd name="1" x="0" y="0" dx="1.1176" dy="1.778" layer="1"/>
<smd name="2" x="2.794" y="0" dx="1.1176" dy="1.778" layer="1"/>
<text x="-0.6604" y="1.2954" size="0.6096" layer="25">&gt;NAME</text>
<text x="-0.6604" y="-1.651" size="0.6096" layer="27">&gt;VALUE</text>
</package>
<package name="SOT223">
<wire x1="-1.143" y1="1.397" x2="-1.143" y2="5.207" width="0.127" layer="51"/>
<wire x1="-1.143" y1="5.207" x2="5.715" y2="5.207" width="0.127" layer="51"/>
<wire x1="5.715" y1="5.207" x2="5.715" y2="1.397" width="0.127" layer="51"/>
<wire x1="5.715" y1="1.397" x2="-1.143" y2="1.397" width="0.127" layer="51"/>
<wire x1="4.572" y1="5.207" x2="5.715" y2="5.207" width="0.127" layer="21"/>
<wire x1="5.715" y1="5.207" x2="5.715" y2="3.683" width="0.127" layer="21"/>
<wire x1="0" y1="1.397" x2="-1.143" y2="1.397" width="0.127" layer="21"/>
<wire x1="-1.143" y1="1.397" x2="-1.143" y2="3.048" width="0.127" layer="21"/>
<smd name="1" x="0" y="0" dx="1.524" dy="2.032" layer="1"/>
<smd name="2" x="2.286" y="0" dx="1.524" dy="2.032" layer="1"/>
<smd name="3" x="4.572" y="0" dx="1.524" dy="2.032" layer="1"/>
<smd name="4" x="2.286" y="6.604" dx="3.81" dy="2.032" layer="1"/>
<text x="-1.524" y="5.715" size="0.6096" layer="25">&gt;NAME</text>
<text x="6.604" y="1.016" size="0.6096" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="LED0805">
<wire x1="0.127" y1="-0.635" x2="0.127" y2="0.635" width="0.127" layer="51"/>
<wire x1="0.127" y1="0.635" x2="2.159" y2="0.635" width="0.127" layer="51"/>
<wire x1="2.159" y1="0.635" x2="2.159" y2="-0.635" width="0.127" layer="51"/>
<wire x1="2.159" y1="-0.635" x2="0.127" y2="-0.635" width="0.127" layer="51"/>
<wire x1="1.143" y1="0.635" x2="1.143" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.016" y1="-0.635" x2="-1.016" y2="0.635" width="0.2032" layer="21"/>
<smd name="1" x="0" y="0" dx="1.524" dy="1.524" layer="1"/>
<smd name="2" x="2.286" y="0" dx="1.524" dy="1.524" layer="1"/>
<text x="-0.889" y="1.016" size="0.6096" layer="25">&gt;NAME</text>
<text x="-0.889" y="-1.651" size="0.6096" layer="27">&gt;VALUE</text>
</package>
<package name="SOD-123">
<wire x1="0.254" y1="-0.889" x2="0.254" y2="0.889" width="0.127" layer="51"/>
<wire x1="0.254" y1="0.889" x2="3.175" y2="0.889" width="0.127" layer="51"/>
<wire x1="3.175" y1="0.889" x2="3.175" y2="-0.889" width="0.127" layer="51"/>
<wire x1="3.175" y1="-0.889" x2="0.254" y2="-0.889" width="0.127" layer="51"/>
<wire x1="1.016" y1="0.889" x2="2.413" y2="0.889" width="0.2032" layer="21"/>
<wire x1="1.016" y1="-0.889" x2="2.54" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="-0.889" y1="0.508" x2="-0.889" y2="-0.508" width="0.2032" layer="21"/>
<smd name="1" x="0" y="0" dx="1.143" dy="1.143" layer="1"/>
<smd name="2" x="3.429" y="0" dx="1.143" dy="1.143" layer="1"/>
<text x="0" y="1.143" size="0.6096" layer="25">&gt;NAME</text>
<text x="0" y="-1.778" size="0.6096" layer="27">&gt;VALUE</text>
</package>
<package name="157SW">
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.127" layer="51"/>
<wire x1="0" y1="1.27" x2="3.9878" y2="1.27" width="0.127" layer="51"/>
<wire x1="3.9878" y1="1.27" x2="3.9878" y2="-1.27" width="0.127" layer="51"/>
<wire x1="3.9878" y1="-1.27" x2="0" y2="-1.27" width="0.127" layer="51"/>
<wire x1="1.016" y1="1.27" x2="3.048" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.016" y1="-1.27" x2="3.048" y2="-1.27" width="0.2032" layer="21"/>
<circle x="2.032" y="0" radius="0.8131" width="0.2032" layer="21"/>
<circle x="2.032" y="0" radius="0.8131" width="0.127" layer="51"/>
<smd name="1" x="0" y="0" dx="1.016" dy="1.9812" layer="1"/>
<smd name="2" x="4.0005" y="0" dx="1.016" dy="1.9812" layer="1"/>
<text x="0" y="1.524" size="0.6096" layer="25">&gt;NAME</text>
<text x="0" y="-2.159" size="0.6096" layer="27">&gt;VALUE</text>
</package>
<package name="RES4NT">
<wire x1="-0.508" y1="0" x2="-0.508" y2="1.651" width="0.127" layer="51"/>
<wire x1="-0.508" y1="1.651" x2="2.921" y2="1.651" width="0.127" layer="51"/>
<wire x1="2.921" y1="1.651" x2="2.921" y2="0" width="0.127" layer="51"/>
<wire x1="2.921" y1="0" x2="-0.508" y2="0" width="0.127" layer="51"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="1.651" width="0.2032" layer="21"/>
<wire x1="2.921" y1="0" x2="2.921" y2="1.651" width="0.2032" layer="21"/>
<smd name="1" x="0" y="0" dx="0.508" dy="1.016" layer="1"/>
<smd name="2" x="0.8001" y="0" dx="0.508" dy="1.016" layer="1"/>
<smd name="3" x="1.6002" y="0" dx="0.508" dy="1.016" layer="1"/>
<smd name="4" x="2.4003" y="0" dx="0.508" dy="1.016" layer="1"/>
<smd name="5" x="2.4003" y="1.651" dx="0.508" dy="1.016" layer="1"/>
<smd name="6" x="1.6002" y="1.651" dx="0.508" dy="1.016" layer="1"/>
<smd name="7" x="0.8001" y="1.651" dx="0.508" dy="1.016" layer="1"/>
<smd name="8" x="0" y="1.651" dx="0.508" dy="1.016" layer="1"/>
<text x="-0.635" y="2.413" size="0.6096" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.27" size="0.6096" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="ATMEGA168-20AU">
<wire x1="-17.78" y1="-27.94" x2="15.24" y2="-27.94" width="0.254" layer="94"/>
<wire x1="15.24" y1="-27.94" x2="15.24" y2="27.94" width="0.254" layer="94"/>
<wire x1="15.24" y1="27.94" x2="-17.78" y2="27.94" width="0.254" layer="94"/>
<wire x1="-17.78" y1="27.94" x2="-17.78" y2="-27.94" width="0.254" layer="94"/>
<text x="-12.7" y="-26.67" size="1.778" layer="96">&gt;VALUE</text>
<text x="-17.78" y="28.956" size="1.778" layer="95">&gt;NAME</text>
<pin name="PD3(INT1)" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="PD2(IND0)" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="PD1(TXD)" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="PD0(RXD)" x="20.32" y="15.24" length="middle" rot="R180"/>
<pin name="VCC2" x="20.32" y="22.86" length="middle" direction="pwr" rot="R180"/>
<pin name="VCC1" x="20.32" y="25.4" length="middle" direction="pwr" rot="R180"/>
<pin name="PC6(RESET)" x="-22.86" y="25.4" length="middle" function="dot"/>
<pin name="PB6(XTAL1)" x="-22.86" y="20.32" length="middle" direction="pas"/>
<pin name="PC1(ADC1)" x="-22.86" y="2.54" length="middle" direction="pas"/>
<pin name="PC2(ADC2)" x="-22.86" y="0" length="middle" direction="pas"/>
<pin name="PC3(ADC3)" x="-22.86" y="-2.54" length="middle" direction="pas"/>
<pin name="PC4(ADC4)" x="-22.86" y="-5.08" length="middle" direction="pas"/>
<pin name="PC5(ADC5)" x="-22.86" y="-7.62" length="middle" direction="pas"/>
<pin name="ADC6" x="-22.86" y="-10.16" length="middle" direction="pas"/>
<pin name="AGND" x="-22.86" y="-17.78" length="middle" direction="pas"/>
<pin name="ADC7" x="-22.86" y="-12.7" length="middle" direction="pas"/>
<pin name="GND2" x="-22.86" y="-22.86" length="middle" direction="pwr"/>
<pin name="GND1" x="-22.86" y="-20.32" length="middle" direction="pwr"/>
<pin name="PC0(ADC0)" x="-22.86" y="5.08" length="middle" direction="pas"/>
<pin name="AREF" x="-22.86" y="10.16" length="middle" direction="pas"/>
<pin name="PB7(XTAL2)" x="-22.86" y="15.24" length="middle" direction="pas"/>
<pin name="AVCC" x="20.32" y="20.32" length="middle" direction="pas" rot="R180"/>
<pin name="PD4(T0)" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="PD5(T1)" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="PD6(AIN0)" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="PD7(AIN1)" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="PB0(ICP)" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="PB1(OC1)" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="PB2(SS)" x="20.32" y="-12.7" length="middle" rot="R180"/>
<pin name="PB3(MOSI)" x="20.32" y="-15.24" length="middle" rot="R180"/>
<pin name="PB4(MISO)" x="20.32" y="-17.78" length="middle" rot="R180"/>
<pin name="PB5(SCK)" x="20.32" y="-20.32" length="middle" rot="R180"/>
</symbol>
<symbol name="CAP-NP">
<wire x1="-2.54" y1="1.016" x2="0" y2="1.016" width="0.254" layer="94"/>
<wire x1="0" y1="1.016" x2="2.54" y2="1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.016" x2="0" y2="-1.016" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="2.54" y2="-1.016" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.016" width="0.1524" layer="94"/>
<text x="2.54" y="1.778" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="CAP-P">
<wire x1="-2.54" y1="1.27" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.254" layer="94" curve="-100.000346"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-0.254" width="0.1524" layer="94"/>
<text x="1.27" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="1.27" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="UA78M05">
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<text x="-10.16" y="8.636" size="1.27" layer="95">&gt;NAME</text>
<text x="-5.08" y="5.08" size="1.27" layer="96">&gt;VALUE</text>
<pin name="IN" x="-15.24" y="2.54" length="middle" direction="pwr"/>
<pin name="GND1" x="-2.54" y="-10.16" length="middle" direction="pwr" rot="R90"/>
<pin name="OUT" x="15.24" y="2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="GND2" x="2.54" y="-10.16" length="middle" direction="pwr" rot="R90"/>
</symbol>
<symbol name="LED">
<wire x1="0" y1="2.032" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.556" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="1.27" x2="2.286" y2="3.302" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.27" x2="4.064" y2="3.302" width="0.1524" layer="94"/>
<text x="-7.62" y="3.302" size="1.27" layer="95">&gt;NAME</text>
<text x="-7.112" y="-4.064" size="1.27" layer="96">&gt;VALUE</text>
<pin name="AN" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<pin name="CA" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<polygon width="0.1524" layer="94">
<vertex x="1.9558" y="3.5306"/>
<vertex x="2.5908" y="3.048"/>
<vertex x="2.794" y="3.8862"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="3.7338" y="3.5306"/>
<vertex x="4.3688" y="3.048"/>
<vertex x="4.572" y="3.8862"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="-3.556" y="1.778"/>
<vertex x="-3.556" y="-1.778"/>
<vertex x="0" y="0"/>
</polygon>
</symbol>
<symbol name="SCHOTTKY">
<wire x1="0" y1="2.032" x2="0" y2="-2.032" width="0.254" layer="94"/>
<wire x1="0" y1="2.032" x2="-1.778" y2="2.032" width="0.254" layer="94"/>
<wire x1="-1.778" y1="2.032" x2="-1.778" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-2.032" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<wire x1="1.778" y1="-2.032" x2="1.778" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.683" y2="0" width="0.1524" layer="94"/>
<text x="-7.112" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="-7.112" y="-4.572" size="1.27" layer="96">&gt;VALUE</text>
<pin name="CA" x="5.08" y="0" visible="off" length="middle" direction="pas" rot="R180"/>
<pin name="AN" x="-10.16" y="0" visible="off" length="middle" direction="pas"/>
<polygon width="0.254" layer="94">
<vertex x="-3.556" y="1.778"/>
<vertex x="-3.556" y="-1.778"/>
<vertex x="0" y="0"/>
</polygon>
</symbol>
<symbol name="PBSWITCH">
<wire x1="-4.572" y1="2.54" x2="-2.286" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.286" y1="2.54" x2="2.286" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.286" y1="2.54" x2="4.572" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.286" y1="2.54" x2="-2.286" y2="4.318" width="0.254" layer="94"/>
<wire x1="-2.286" y1="4.318" x2="2.286" y2="4.318" width="0.254" layer="94"/>
<wire x1="2.286" y1="4.318" x2="2.286" y2="2.54" width="0.254" layer="94"/>
<text x="-8.636" y="4.826" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.318" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="0" visible="off" length="middle" direction="pas" function="dot"/>
<pin name="2" x="7.62" y="0" visible="off" length="middle" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="RES4NT">
<wire x1="-2.54" y1="0" x2="-2.032" y2="-1.016" width="0.1778" layer="94"/>
<wire x1="-2.032" y1="-1.016" x2="-1.016" y2="1.016" width="0.1778" layer="94"/>
<wire x1="-1.016" y1="1.016" x2="0" y2="-1.016" width="0.1778" layer="94"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="1.016" width="0.1778" layer="94"/>
<wire x1="1.016" y1="1.016" x2="2.032" y2="-1.016" width="0.1778" layer="94"/>
<wire x1="2.032" y1="-1.016" x2="2.54" y2="0" width="0.1778" layer="94"/>
<text x="-2.286" y="1.27" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.366" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATMEGA168-20AU" prefix="U">
<gates>
<gate name="G$1" symbol="ATMEGA168-20AU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFP032">
<connects>
<connect gate="G$1" pin="ADC6" pad="19"/>
<connect gate="G$1" pin="ADC7" pad="22"/>
<connect gate="G$1" pin="AGND" pad="21"/>
<connect gate="G$1" pin="AREF" pad="20"/>
<connect gate="G$1" pin="AVCC" pad="18"/>
<connect gate="G$1" pin="GND1" pad="3"/>
<connect gate="G$1" pin="GND2" pad="5"/>
<connect gate="G$1" pin="PB0(ICP)" pad="12"/>
<connect gate="G$1" pin="PB1(OC1)" pad="13"/>
<connect gate="G$1" pin="PB2(SS)" pad="14"/>
<connect gate="G$1" pin="PB3(MOSI)" pad="15"/>
<connect gate="G$1" pin="PB4(MISO)" pad="16"/>
<connect gate="G$1" pin="PB5(SCK)" pad="17"/>
<connect gate="G$1" pin="PB6(XTAL1)" pad="7"/>
<connect gate="G$1" pin="PB7(XTAL2)" pad="8"/>
<connect gate="G$1" pin="PC0(ADC0)" pad="23"/>
<connect gate="G$1" pin="PC1(ADC1)" pad="24"/>
<connect gate="G$1" pin="PC2(ADC2)" pad="25"/>
<connect gate="G$1" pin="PC3(ADC3)" pad="26"/>
<connect gate="G$1" pin="PC4(ADC4)" pad="27"/>
<connect gate="G$1" pin="PC5(ADC5)" pad="28"/>
<connect gate="G$1" pin="PC6(RESET)" pad="29"/>
<connect gate="G$1" pin="PD0(RXD)" pad="30"/>
<connect gate="G$1" pin="PD1(TXD)" pad="31"/>
<connect gate="G$1" pin="PD2(IND0)" pad="32"/>
<connect gate="G$1" pin="PD3(INT1)" pad="1"/>
<connect gate="G$1" pin="PD4(T0)" pad="2"/>
<connect gate="G$1" pin="PD5(T1)" pad="9"/>
<connect gate="G$1" pin="PD6(AIN0)" pad="10"/>
<connect gate="G$1" pin="PD7(AIN1)" pad="11"/>
<connect gate="G$1" pin="VCC1" pad="4"/>
<connect gate="G$1" pin="VCC2" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP0805-NP" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAP-NP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAP0805-NP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP-TANA-P" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAP-P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TAN-A">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="UA78M05" prefix="U">
<gates>
<gate name="G$1" symbol="UA78M05" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT223">
<connects>
<connect gate="G$1" pin="GND1" pad="2"/>
<connect gate="G$1" pin="GND2" pad="4"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED0805" prefix="LED" uservalue="yes">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED0805">
<connects>
<connect gate="G$1" pin="AN" pad="2"/>
<connect gate="G$1" pin="CA" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MBR0520" prefix="D">
<gates>
<gate name="G$1" symbol="SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD-123">
<connects>
<connect gate="G$1" pin="AN" pad="2"/>
<connect gate="G$1" pin="CA" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PB157" prefix="SW" uservalue="yes">
<gates>
<gate name="G$1" symbol="PBSWITCH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="157SW">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES4NT" prefix="RP" uservalue="yes">
<gates>
<gate name="A" symbol="RES4NT" x="0" y="0" swaplevel="1"/>
<gate name="B" symbol="RES4NT" x="0" y="-7.62" swaplevel="1"/>
<gate name="C" symbol="RES4NT" x="0" y="-15.24" swaplevel="1"/>
<gate name="D" symbol="RES4NT" x="0" y="-22.86" swaplevel="1"/>
</gates>
<devices>
<device name="" package="RES4NT">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="8"/>
<connect gate="B" pin="1" pad="2"/>
<connect gate="B" pin="2" pad="7"/>
<connect gate="C" pin="1" pad="3"/>
<connect gate="C" pin="2" pad="6"/>
<connect gate="D" pin="1" pad="4"/>
<connect gate="D" pin="2" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<packages>
</packages>
<symbols>
<symbol name="FRAME_B_L">
<frame x1="0" y1="0" x2="431.8" y2="279.4" columns="9" rows="6" layer="94" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94" font="vector">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94" font="vector">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94" font="vector">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94" font="vector">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="FRAME_B_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt; B Size , 11 x 17 INCH, Landscape&lt;p&gt;</description>
<gates>
<gate name="G$1" symbol="FRAME_B_L" x="0" y="0" addlevel="always"/>
<gate name="G$2" symbol="DOCFIELD" x="325.12" y="0" addlevel="always"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MyPOW">
<packages>
</packages>
<symbols>
<symbol name="COM">
<wire x1="-1.524" y1="0" x2="1.524" y2="0" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-0.508" x2="1.016" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.016" x2="0.508" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-0.127" y1="-1.524" x2="0.127" y2="-1.524" width="0.254" layer="94"/>
<pin name="COM" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+5V">
<wire x1="-1.016" y1="0" x2="1.016" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="0" x2="0" y2="1.778" width="0.254" layer="94"/>
<wire x1="0" y1="1.778" x2="-1.016" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="VIN">
<wire x1="-1.016" y1="0" x2="1.016" y2="0" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.778" width="0.254" layer="94"/>
<wire x1="0" y1="1.778" x2="1.016" y2="0" width="0.254" layer="94"/>
<text x="-2.032" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VIN" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="3V3">
<wire x1="-1.016" y1="0" x2="1.016" y2="0" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.651" width="0.254" layer="94"/>
<wire x1="0" y1="1.651" x2="1.016" y2="0" width="0.254" layer="94"/>
<text x="-2.286" y="2.286" size="1.778" layer="96">&gt;VALUE</text>
<pin name="3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="VUSB">
<wire x1="-1.016" y1="0" x2="1.016" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="0" x2="0" y2="1.651" width="0.254" layer="94"/>
<wire x1="0" y1="1.651" x2="-1.016" y2="0" width="0.254" layer="94"/>
<text x="-3.048" y="2.032" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VUSB" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="NC">
<wire x1="0" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.397" y1="0.635" x2="-0.127" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-0.127" y1="0.635" x2="-1.397" y2="-0.635" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="COM">
<gates>
<gate name="G$1" symbol="COM" x="0" y="-2.54"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V">
<gates>
<gate name="G$1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VIN">
<gates>
<gate name="G$1" symbol="VIN" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3V3">
<gates>
<gate name="G$1" symbol="3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VUSB">
<gates>
<gate name="G$1" symbol="VUSB" x="0" y="2.54"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NC">
<gates>
<gate name="G$1" symbol="NC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MySMD2">
<packages>
<package name="SSOP28">
<wire x1="-0.762" y1="0.635" x2="-0.762" y2="6.604" width="0.127" layer="51"/>
<wire x1="-0.762" y1="6.604" x2="9.144" y2="6.604" width="0.127" layer="51"/>
<wire x1="9.144" y1="6.604" x2="9.144" y2="0.635" width="0.127" layer="51"/>
<wire x1="9.144" y1="0.635" x2="-0.762" y2="0.635" width="0.127" layer="51"/>
<wire x1="-0.762" y1="0.635" x2="-0.762" y2="6.604" width="0.2032" layer="21"/>
<wire x1="9.144" y1="0.635" x2="9.144" y2="6.604" width="0.2032" layer="21"/>
<wire x1="-0.381" y1="2.032" x2="0.381" y2="2.032" width="0.2032" layer="21"/>
<wire x1="0.381" y1="2.032" x2="0" y2="1.397" width="0.2032" layer="21"/>
<wire x1="0" y1="1.397" x2="-0.381" y2="2.032" width="0.2032" layer="21"/>
<smd name="1" x="0" y="0" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="2" x="0.6502" y="0" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="3" x="1.3" y="0" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="4" x="1.95" y="0" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="5" x="2.6002" y="0" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="6" x="3.2499" y="0" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="7" x="3.8997" y="0" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="8" x="4.5499" y="0" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="9" x="5.1999" y="0" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="10" x="5.8501" y="0" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="11" x="6.5004" y="0" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="12" x="7.1501" y="0" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="13" x="7.7998" y="0" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="14" x="8.4501" y="0" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="15" x="8.4501" y="7.112" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="16" x="7.7998" y="7.112" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="17" x="7.1501" y="7.112" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="18" x="6.5004" y="7.112" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="19" x="5.8501" y="7.112" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="20" x="5.1999" y="7.112" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="21" x="4.5499" y="7.112" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="22" x="3.8997" y="7.112" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="23" x="3.2499" y="7.112" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="24" x="2.6002" y="7.112" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="25" x="1.95" y="7.112" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="26" x="1.3" y="7.112" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="27" x="0.6497" y="7.112" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<smd name="28" x="0" y="7.112" dx="0.381" dy="2.032" layer="1" roundness="100"/>
<text x="-0.762" y="8.382" size="0.6096" layer="25">&gt;NAME</text>
<text x="-0.762" y="-1.905" size="0.6096" layer="27">&gt;VALUE</text>
</package>
<package name="RESONATOR">
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.8" x2="1.4" y2="-0.8" width="0.127" layer="51"/>
<wire x1="1.4" y1="-0.8" x2="-1.4" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-1.4" y1="-0.8" x2="-1.4" y2="0.8" width="0.127" layer="51"/>
<wire x1="-1.6" y1="0.8" x2="-1.6" y2="-0.8" width="0.22" layer="21"/>
<wire x1="1.6" y1="0.8" x2="1.6" y2="-0.8" width="0.22" layer="21"/>
<smd name="2" x="0" y="0" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="3" x="1" y="0" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="1" x="-1" y="0" dx="2" dy="0.7" layer="1" rot="R90"/>
<text x="-1.45" y="1.35" size="0.3556" layer="25">&gt;NAME</text>
<text x="-1.45" y="-1.688" size="0.3556" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="FL232RL">
<wire x1="-12.7" y1="25.4" x2="-12.7" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-25.4" x2="12.7" y2="-25.4" width="0.254" layer="94"/>
<wire x1="12.7" y1="-25.4" x2="12.7" y2="25.4" width="0.254" layer="94"/>
<wire x1="12.7" y1="25.4" x2="-12.7" y2="25.4" width="0.254" layer="94"/>
<text x="-12.7" y="26.416" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-24.384" size="1.778" layer="96">&gt;VALUE</text>
<pin name="CTS#" x="-17.78" y="-2.54" length="middle" direction="in"/>
<pin name="DCD#" x="-17.78" y="0" length="middle" direction="in"/>
<pin name="DSR#" x="-17.78" y="2.54" length="middle" direction="in"/>
<pin name="NC1" x="-17.78" y="5.08" length="middle" direction="nc"/>
<pin name="RI#" x="-17.78" y="7.62" length="middle" direction="in"/>
<pin name="RXD" x="-17.78" y="10.16" length="middle" direction="in"/>
<pin name="VCCIO" x="-17.78" y="12.7" length="middle" direction="pwr"/>
<pin name="RTS#" x="-17.78" y="15.24" length="middle" direction="out"/>
<pin name="DTR#" x="-17.78" y="17.78" length="middle" direction="out"/>
<pin name="TXD" x="-17.78" y="20.32" length="middle" direction="out"/>
<pin name="TEST" x="-17.78" y="-7.62" length="middle" direction="in"/>
<pin name="AGND" x="-17.78" y="-12.7" length="middle" direction="pwr"/>
<pin name="GND1" x="-17.78" y="-15.24" length="middle" direction="pwr"/>
<pin name="GND2" x="-17.78" y="-17.78" length="middle" direction="pwr"/>
<pin name="GND3" x="-17.78" y="-20.32" length="middle" direction="pwr"/>
<pin name="USBDP" x="17.78" y="-20.32" length="middle" rot="R180"/>
<pin name="USBDM" x="17.78" y="-17.78" length="middle" rot="R180"/>
<pin name="3V3OUT" x="17.78" y="-12.7" length="middle" direction="pwr" rot="R180"/>
<pin name="RESET#" x="17.78" y="-7.62" length="middle" direction="in" rot="R180"/>
<pin name="CBUS4" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="CBUS3" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="CBUS2" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="CBUS1" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="CBUS0" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="NC2" x="17.78" y="10.16" length="middle" direction="nc" rot="R180"/>
<pin name="OSCI" x="17.78" y="12.7" length="middle" direction="in" rot="R180"/>
<pin name="VCC" x="17.78" y="20.32" length="middle" direction="pwr" rot="R180"/>
<pin name="OSCO" x="17.78" y="15.24" length="middle" direction="out" rot="R180"/>
</symbol>
<symbol name="RESONATOR">
<wire x1="-1.524" y1="-0.508" x2="1.524" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-0.508" x2="1.524" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.524" y1="0.508" x2="-1.524" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0.508" x2="-1.524" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-1.016" x2="0" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.016" x2="1.778" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="1.016" x2="0" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="1.016" x2="1.778" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="1.016" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-2.032" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-2.032" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="3.302" x2="-2.032" y2="1.778" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="3.302" x2="-3.048" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.778" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-1.778" x2="-2.032" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="-1.778" x2="-3.048" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-3.048" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<text x="-5.08" y="4.572" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-5.588" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="-7.62" y="0" visible="off" length="point" direction="pas"/>
<pin name="1" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R180"/>
<pin name="3" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FT232RL" prefix="U">
<gates>
<gate name="G$1" symbol="FL232RL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SSOP28">
<connects>
<connect gate="G$1" pin="3V3OUT" pad="17"/>
<connect gate="G$1" pin="AGND" pad="25"/>
<connect gate="G$1" pin="CBUS0" pad="23"/>
<connect gate="G$1" pin="CBUS1" pad="22"/>
<connect gate="G$1" pin="CBUS2" pad="13"/>
<connect gate="G$1" pin="CBUS3" pad="14"/>
<connect gate="G$1" pin="CBUS4" pad="12"/>
<connect gate="G$1" pin="CTS#" pad="11"/>
<connect gate="G$1" pin="DCD#" pad="10"/>
<connect gate="G$1" pin="DSR#" pad="9"/>
<connect gate="G$1" pin="DTR#" pad="2"/>
<connect gate="G$1" pin="GND1" pad="7"/>
<connect gate="G$1" pin="GND2" pad="18"/>
<connect gate="G$1" pin="GND3" pad="21"/>
<connect gate="G$1" pin="NC1" pad="8"/>
<connect gate="G$1" pin="NC2" pad="24"/>
<connect gate="G$1" pin="OSCI" pad="27"/>
<connect gate="G$1" pin="OSCO" pad="28"/>
<connect gate="G$1" pin="RESET#" pad="19"/>
<connect gate="G$1" pin="RI#" pad="6"/>
<connect gate="G$1" pin="RTS#" pad="3"/>
<connect gate="G$1" pin="RXD" pad="5"/>
<connect gate="G$1" pin="TEST" pad="26"/>
<connect gate="G$1" pin="TXD" pad="1"/>
<connect gate="G$1" pin="USBDM" pad="16"/>
<connect gate="G$1" pin="USBDP" pad="15"/>
<connect gate="G$1" pin="VCC" pad="20"/>
<connect gate="G$1" pin="VCCIO" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESONATOR" prefix="Y">
<gates>
<gate name="G$1" symbol="RESONATOR" x="0" y="0"/>
</gates>
<devices>
<device name="MU" package="RESONATOR">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MyCON2">
<packages>
<package name="USB-MINI-B">
<wire x1="-4.445" y1="-6.35" x2="-4.445" y2="1.905" width="0.127" layer="51"/>
<wire x1="-4.445" y1="1.905" x2="7.62" y2="1.905" width="0.127" layer="51"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-6.35" width="0.127" layer="51"/>
<wire x1="7.62" y1="-6.35" x2="5.461" y2="-6.35" width="0.127" layer="51"/>
<wire x1="5.461" y1="-6.35" x2="5.461" y2="-8.382" width="0.127" layer="51"/>
<wire x1="5.461" y1="-8.382" x2="-2.286" y2="-8.382" width="0.127" layer="51"/>
<wire x1="-2.286" y1="-8.382" x2="-2.286" y2="-6.35" width="0.127" layer="51"/>
<wire x1="-2.286" y1="-6.35" x2="-4.445" y2="-6.35" width="0.127" layer="51"/>
<wire x1="-0.508" y1="-6.35" x2="4.064" y2="-6.35" width="0.127" layer="51"/>
<wire x1="-0.508" y1="-6.35" x2="4.064" y2="-6.35" width="0.2032" layer="21"/>
<smd name="1" x="0" y="0" dx="0.508" dy="2.286" layer="1"/>
<smd name="2" x="0.8001" y="0" dx="0.508" dy="2.286" layer="1"/>
<smd name="3" x="1.6002" y="0" dx="0.508" dy="2.286" layer="1"/>
<smd name="4" x="2.4003" y="0" dx="0.508" dy="2.286" layer="1"/>
<smd name="5" x="3.2004" y="0" dx="0.508" dy="2.286" layer="1"/>
<smd name="6" x="-3.3282" y="0" dx="2.0574" dy="2.2352" layer="1"/>
<smd name="7" x="6.5286" y="0" dx="2.0574" dy="2.2352" layer="1"/>
<smd name="8" x="-3.3282" y="-4.2499" dx="2.0574" dy="2.9718" layer="1"/>
<smd name="9" x="6.5286" y="-4.2499" dx="2.0574" dy="2.9718" layer="1"/>
<text x="-4.191" y="2.159" size="0.6096" layer="25">&gt;NAME</text>
<text x="-4.191" y="-9.144" size="0.6096" layer="27">&gt;VALUE</text>
<hole x="-0.5999" y="-2.2499" drill="0.8636"/>
<hole x="3.8003" y="-2.2499" drill="0.8636"/>
</package>
<package name="USB-MINI-B_2">
<wire x1="-4.064" y1="-1.524" x2="-4.064" y2="6.985" width="0.127" layer="51"/>
<wire x1="-4.064" y1="6.985" x2="-1.397" y2="6.985" width="0.127" layer="51"/>
<wire x1="-1.397" y1="6.985" x2="-1.397" y2="8.89" width="0.127" layer="51"/>
<wire x1="-1.397" y1="8.89" x2="4.572" y2="8.89" width="0.127" layer="51"/>
<wire x1="4.572" y1="8.89" x2="4.572" y2="6.985" width="0.127" layer="51"/>
<wire x1="4.572" y1="6.985" x2="7.239" y2="6.985" width="0.127" layer="51"/>
<wire x1="7.239" y1="6.985" x2="7.239" y2="-1.524" width="0.127" layer="51"/>
<wire x1="7.239" y1="-1.524" x2="-4.064" y2="-1.524" width="0.127" layer="51"/>
<wire x1="0" y1="6.985" x2="3.175" y2="6.985" width="0.127" layer="51"/>
<wire x1="0" y1="6.985" x2="3.175" y2="6.985" width="0.127" layer="21"/>
<smd name="1" x="0" y="0" dx="2.54" dy="0.508" layer="1" rot="R90"/>
<smd name="2" x="0.8" y="0" dx="2.54" dy="0.508" layer="1" rot="R90"/>
<smd name="3" x="1.6" y="0" dx="2.54" dy="0.508" layer="1" rot="R90"/>
<smd name="4" x="2.4" y="0" dx="2.54" dy="0.508" layer="1" rot="R90"/>
<smd name="5" x="3.2" y="0" dx="2.54" dy="0.508" layer="1" rot="R90"/>
<smd name="6" x="-2.7175" y="5.948" dx="1.778" dy="2.286" layer="1" rot="R90"/>
<smd name="7" x="-2.7175" y="0.36" dx="1.778" dy="2.286" layer="1" rot="R90"/>
<smd name="9" x="5.9185" y="5.948" dx="1.778" dy="2.286" layer="1" rot="R90"/>
<smd name="8" x="5.9185" y="0.36" dx="1.778" dy="2.286" layer="1" rot="R90"/>
<text x="-4.064" y="-3.175" size="1.27" layer="25" ratio="14">&gt;NAME</text>
<text x="-4.318" y="-1.524" size="1.27" layer="27" ratio="14" rot="R90">&gt;VALUE</text>
<hole x="-0.6" y="2.9" drill="0.8128"/>
<hole x="3.8" y="2.9" drill="0.8128"/>
</package>
<package name="HEAD15-NOSS">
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.127" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="36.83" y2="1.27" width="0.127" layer="51"/>
<wire x1="36.83" y1="1.27" x2="36.83" y2="-1.27" width="0.127" layer="51"/>
<wire x1="36.83" y1="-1.27" x2="-1.27" y2="-1.27" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9144" diameter="1.778" shape="square"/>
<pad name="2" x="2.54" y="0" drill="0.9144" diameter="1.778"/>
<pad name="3" x="5.08" y="0" drill="0.9144" diameter="1.778"/>
<pad name="4" x="7.62" y="0" drill="0.9144" diameter="1.778"/>
<pad name="5" x="10.16" y="0" drill="0.9144" diameter="1.778"/>
<pad name="6" x="12.7" y="0" drill="0.9144" diameter="1.778"/>
<pad name="7" x="15.24" y="0" drill="0.9144" diameter="1.778"/>
<pad name="8" x="17.78" y="0" drill="0.9144" diameter="1.778"/>
<pad name="9" x="20.32" y="0" drill="0.9144" diameter="1.778"/>
<pad name="10" x="22.86" y="0" drill="0.9144" diameter="1.778"/>
<pad name="11" x="25.4" y="0" drill="0.9144" diameter="1.778"/>
<pad name="12" x="27.94" y="0" drill="0.9144" diameter="1.778"/>
<pad name="13" x="30.48" y="0" drill="0.9144" diameter="1.778"/>
<pad name="14" x="33.02" y="0" drill="0.9144" diameter="1.778"/>
<pad name="15" x="35.56" y="0" drill="0.9144" diameter="1.778"/>
<text x="-1.27" y="1.651" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.2446" y="-2.1082" size="0.6096" layer="27">&gt;VALUE</text>
</package>
<package name="HEAD15-NOSS-1">
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.127" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="36.83" y2="1.27" width="0.127" layer="51"/>
<wire x1="36.83" y1="1.27" x2="36.83" y2="-1.27" width="0.127" layer="51"/>
<wire x1="36.83" y1="-1.27" x2="-1.27" y2="-1.27" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9144" diameter="1.778"/>
<pad name="2" x="2.54" y="0" drill="0.9144" diameter="1.778"/>
<pad name="3" x="5.08" y="0" drill="0.9144" diameter="1.778"/>
<pad name="4" x="7.62" y="0" drill="0.9144" diameter="1.778"/>
<pad name="5" x="10.16" y="0" drill="0.9144" diameter="1.778"/>
<pad name="6" x="12.7" y="0" drill="0.9144" diameter="1.778"/>
<pad name="7" x="15.24" y="0" drill="0.9144" diameter="1.778"/>
<pad name="8" x="17.78" y="0" drill="0.9144" diameter="1.778"/>
<pad name="9" x="20.32" y="0" drill="0.9144" diameter="1.778"/>
<pad name="10" x="22.86" y="0" drill="0.9144" diameter="1.778"/>
<pad name="11" x="25.4" y="0" drill="0.9144" diameter="1.778"/>
<pad name="12" x="27.94" y="0" drill="0.9144" diameter="1.778"/>
<pad name="13" x="30.48" y="0" drill="0.9144" diameter="1.778"/>
<pad name="14" x="33.02" y="0" drill="0.9144" diameter="1.778"/>
<pad name="15" x="35.56" y="0" drill="0.9144" diameter="1.778"/>
<text x="-1.27" y="1.5748" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.2954" y="-2.1082" size="0.6096" layer="27">&gt;VALUE</text>
</package>
<package name="HEAD3X2">
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="3.81" width="0.127" layer="51"/>
<wire x1="-1.27" y1="3.81" x2="6.35" y2="3.81" width="0.127" layer="51"/>
<wire x1="6.35" y1="3.81" x2="6.35" y2="-1.27" width="0.127" layer="51"/>
<wire x1="6.35" y1="-1.27" x2="-1.27" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.81" x2="6.35" y2="3.81" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.81" x2="6.35" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.9144" diameter="1.778" shape="square"/>
<pad name="3" x="2.54" y="0" drill="0.9144" diameter="1.778"/>
<pad name="5" x="5.08" y="0" drill="0.9144" diameter="1.778"/>
<pad name="2" x="0" y="2.54" drill="0.9144" diameter="1.778"/>
<pad name="4" x="2.54" y="2.54" drill="0.9144" diameter="1.778"/>
<pad name="6" x="5.08" y="2.54" drill="0.9144" diameter="1.778"/>
<text x="-1.27" y="4.064" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="USB">
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<text x="-7.62" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VBUS" x="-12.7" y="5.08" length="middle" direction="pwr"/>
<pin name="D-" x="-12.7" y="2.54" length="middle"/>
<pin name="D+" x="-12.7" y="0" length="middle"/>
<pin name="ID" x="-12.7" y="-2.54" length="middle"/>
<pin name="GND" x="-12.7" y="-5.08" length="middle" direction="pas"/>
<pin name="SH4" x="12.7" y="-5.08" length="middle" direction="pas" rot="R180"/>
<pin name="SH3" x="12.7" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="SH2" x="12.7" y="0" length="middle" direction="pas" rot="R180"/>
<pin name="SH1" x="12.7" y="2.54" length="middle" direction="pas" rot="R180"/>
</symbol>
<symbol name="HEAD15-NOSS">
<wire x1="-2.54" y1="-20.32" x2="5.08" y2="-20.32" width="0.254" layer="94"/>
<wire x1="5.08" y1="-20.32" x2="5.08" y2="20.32" width="0.254" layer="94"/>
<wire x1="5.08" y1="20.32" x2="-2.54" y2="20.32" width="0.254" layer="94"/>
<wire x1="-2.54" y1="20.32" x2="-2.54" y2="-20.32" width="0.254" layer="94"/>
<text x="-2.54" y="-22.86" size="1.27" layer="96">&gt;VALUE</text>
<text x="-2.54" y="21.336" size="1.27" layer="95">&gt;NAME</text>
<pin name="1" x="10.16" y="17.78" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="2" x="10.16" y="15.24" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="3" x="10.16" y="12.7" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="4" x="10.16" y="10.16" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="5" x="10.16" y="7.62" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="6" x="10.16" y="5.08" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="7" x="10.16" y="2.54" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="8" x="10.16" y="0" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="9" x="10.16" y="-2.54" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="10" x="10.16" y="-5.08" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="11" x="10.16" y="-7.62" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="12" x="10.16" y="-10.16" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="13" x="10.16" y="-12.7" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="14" x="10.16" y="-15.24" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="15" x="10.16" y="-17.78" visible="pin" length="middle" direction="pas" rot="R180"/>
</symbol>
<symbol name="HEAD3X2">
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<text x="-5.08" y="8.382" size="1.27" layer="95">&gt;NAME</text>
<text x="-5.08" y="-9.398" size="1.27" layer="96">&gt;VALUE</text>
<pin name="1" x="-10.16" y="5.08" visible="pin" length="middle" direction="pas"/>
<pin name="3" x="-10.16" y="0" visible="pin" length="middle" direction="pas"/>
<pin name="5" x="-10.16" y="-5.08" visible="pin" length="middle" direction="pas"/>
<pin name="6" x="10.16" y="-5.08" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="4" x="10.16" y="0" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="2" x="10.16" y="5.08" visible="pin" length="middle" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="USB-MINI-B" prefix="J">
<gates>
<gate name="G$1" symbol="USB" x="0" y="0"/>
</gates>
<devices>
<device name="%M" package="USB-MINI-B">
<connects>
<connect gate="G$1" pin="D+" pad="3"/>
<connect gate="G$1" pin="D-" pad="2"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="ID" pad="4"/>
<connect gate="G$1" pin="SH1" pad="6"/>
<connect gate="G$1" pin="SH2" pad="7"/>
<connect gate="G$1" pin="SH3" pad="8"/>
<connect gate="G$1" pin="SH4" pad="9"/>
<connect gate="G$1" pin="VBUS" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="%C" package="USB-MINI-B_2">
<connects>
<connect gate="G$1" pin="D+" pad="3"/>
<connect gate="G$1" pin="D-" pad="4"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="ID" pad="2"/>
<connect gate="G$1" pin="SH1" pad="6"/>
<connect gate="G$1" pin="SH2" pad="7"/>
<connect gate="G$1" pin="SH3" pad="8"/>
<connect gate="G$1" pin="SH4" pad="9"/>
<connect gate="G$1" pin="VBUS" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEAD15-NOSS" prefix="J">
<gates>
<gate name="G$1" symbol="HEAD15-NOSS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HEAD15-NOSS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEAD15-NOSS-1" prefix="J">
<gates>
<gate name="G$1" symbol="HEAD15-NOSS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HEAD15-NOSS-1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEAD3X2" prefix="J">
<gates>
<gate name="G$1" symbol="HEAD3X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HEAD3X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit">
<packages>
<package name="SO16W">
<description>&lt;b&gt;SMALL OUTLINE INTEGRATED CIRCUIT&lt;/b&gt;&lt;p&gt;
wide body 7.5 mm/JEDEC MS-013AA</description>
<wire x1="-5.395" y1="5.9" x2="5.395" y2="5.9" width="0.1998" layer="39"/>
<wire x1="5.395" y1="-5.9" x2="-5.395" y2="-5.9" width="0.1998" layer="39"/>
<wire x1="-5.395" y1="-5.9" x2="-5.395" y2="5.9" width="0.1998" layer="39"/>
<wire x1="5.19" y1="-3.7" x2="-5.19" y2="-3.7" width="0.2032" layer="51"/>
<wire x1="-5.19" y1="-3.7" x2="-5.19" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="-5.19" y1="-3.2" x2="-5.19" y2="3.7" width="0.2032" layer="51"/>
<wire x1="-5.19" y1="3.7" x2="5.19" y2="3.7" width="0.2032" layer="51"/>
<wire x1="5.19" y1="-3.2" x2="-5.19" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="5.19" y1="3.7" x2="5.19" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="5.19" y1="-3.2" x2="5.19" y2="-3.7" width="0.2032" layer="51"/>
<wire x1="5.395" y1="5.9" x2="5.395" y2="-5.9" width="0.1998" layer="39"/>
<smd name="2" x="-3.175" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="13" x="-0.635" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-4.445" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="-1.905" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="-0.635" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="14" x="-1.905" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="12" x="0.635" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="11" x="1.905" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="1.905" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="9" x="4.445" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="0.635" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="3.175" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="10" x="3.175" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="4.445" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="15" x="-3.175" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="16" x="-4.445" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<text x="-4.445" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.445" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.6901" y1="-5.32" x2="-4.1999" y2="-3.8001" layer="51"/>
<rectangle x1="-3.4201" y1="-5.32" x2="-2.9299" y2="-3.8001" layer="51"/>
<rectangle x1="-2.1501" y1="-5.32" x2="-1.6599" y2="-3.8001" layer="51"/>
<rectangle x1="-0.8801" y1="-5.32" x2="-0.3899" y2="-3.8001" layer="51"/>
<rectangle x1="0.3899" y1="-5.32" x2="0.8801" y2="-3.8001" layer="51"/>
<rectangle x1="1.6599" y1="-5.32" x2="2.1501" y2="-3.8001" layer="51"/>
<rectangle x1="2.9299" y1="-5.32" x2="3.4201" y2="-3.8001" layer="51"/>
<rectangle x1="4.1999" y1="-5.32" x2="4.6901" y2="-3.8001" layer="51"/>
<rectangle x1="4.1999" y1="3.8001" x2="4.6901" y2="5.32" layer="51"/>
<rectangle x1="2.9299" y1="3.8001" x2="3.4201" y2="5.32" layer="51"/>
<rectangle x1="1.6599" y1="3.8001" x2="2.1501" y2="5.32" layer="51"/>
<rectangle x1="0.3899" y1="3.8001" x2="0.8801" y2="5.32" layer="51"/>
<rectangle x1="-0.8801" y1="3.8001" x2="-0.3899" y2="5.32" layer="51"/>
<rectangle x1="-2.1501" y1="3.8001" x2="-1.6599" y2="5.32" layer="51"/>
<rectangle x1="-3.4201" y1="3.8001" x2="-2.9299" y2="5.32" layer="51"/>
<rectangle x1="-4.6901" y1="3.8001" x2="-4.1999" y2="5.32" layer="51"/>
</package>
<package name="LCD1602">
<wire x1="0" y1="0" x2="80" y2="0" width="0.127" layer="21"/>
<wire x1="80" y1="0" x2="80" y2="36" width="0.127" layer="21"/>
<wire x1="80" y1="36" x2="0" y2="36" width="0.127" layer="21"/>
<wire x1="0" y1="36" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="4" y1="30" x2="76" y2="30" width="0.127" layer="21"/>
<wire x1="76" y1="30" x2="76" y2="6" width="0.127" layer="21"/>
<wire x1="76" y1="6" x2="4" y2="6" width="0.127" layer="21"/>
<wire x1="4" y1="6" x2="4" y2="30" width="0.127" layer="21"/>
<wire x1="6" y1="28" x2="74" y2="28" width="0.127" layer="21"/>
<wire x1="74" y1="28" x2="74" y2="27" width="0.127" layer="21"/>
<wire x1="74" y1="27" x2="6" y2="27" width="0.127" layer="21"/>
<wire x1="6" y1="27" x2="6" y2="28" width="0.127" layer="21"/>
<wire x1="6" y1="8" x2="74" y2="8" width="0.127" layer="21"/>
<wire x1="74" y1="8" x2="74" y2="9" width="0.127" layer="21"/>
<wire x1="74" y1="9" x2="6" y2="9" width="0.127" layer="21"/>
<wire x1="6" y1="9" x2="6" y2="8" width="0.127" layer="21"/>
<wire x1="11" y1="25" x2="8" y2="22" width="0.127" layer="21" curve="90"/>
<wire x1="8" y1="22" x2="8" y2="14" width="0.127" layer="21"/>
<wire x1="8" y1="14" x2="11" y2="11" width="0.127" layer="21" curve="90"/>
<wire x1="11" y1="11" x2="69" y2="11" width="0.127" layer="21"/>
<wire x1="69" y1="11" x2="72" y2="14" width="0.127" layer="21" curve="90"/>
<wire x1="72" y1="14" x2="72" y2="22" width="0.127" layer="21"/>
<wire x1="72" y1="22" x2="69" y2="25" width="0.127" layer="21" curve="90"/>
<wire x1="69" y1="25" x2="11" y2="25" width="0.127" layer="21"/>
<pad name="1" x="8" y="33.46" drill="0.8" shape="long" rot="R90"/>
<pad name="2" x="10.54" y="33.46" drill="0.8" shape="long" rot="R90"/>
<pad name="3" x="13.08" y="33.46" drill="0.8" shape="long" rot="R90"/>
<pad name="4" x="15.62" y="33.46" drill="0.8" shape="long" rot="R90"/>
<pad name="5" x="18.16" y="33.46" drill="0.8" shape="long" rot="R90"/>
<pad name="6" x="20.7" y="33.46" drill="0.8" shape="long" rot="R90"/>
<pad name="7" x="23.24" y="33.46" drill="0.8" shape="long" rot="R90"/>
<pad name="8" x="25.78" y="33.46" drill="0.8" shape="long" rot="R90"/>
<pad name="9" x="28.32" y="33.46" drill="0.8" shape="long" rot="R90"/>
<pad name="10" x="30.86" y="33.46" drill="0.8" shape="long" rot="R90"/>
<pad name="11" x="33.4" y="33.46" drill="0.8" shape="long" rot="R90"/>
<pad name="12" x="35.94" y="33.46" drill="0.8" shape="long" rot="R90"/>
<pad name="13" x="38.48" y="33.46" drill="0.8" shape="long" rot="R90"/>
<pad name="14" x="41.02" y="33.46" drill="0.8" shape="long" rot="R90"/>
<pad name="15" x="43.56" y="33.46" drill="0.8" shape="long" rot="R90"/>
<pad name="16" x="46.1" y="33.46" drill="0.8" shape="long" rot="R90"/>
<hole x="2.54" y="2.54" drill="2.8"/>
<hole x="2.54" y="33.46" drill="2.8"/>
<hole x="77.46" y="33.46" drill="2.8"/>
<hole x="77.46" y="2.54" drill="2.8"/>
</package>
<package name="TC33X">
<wire x1="-1.45" y1="1.75" x2="-1.45" y2="-1.65" width="0.254" layer="51"/>
<wire x1="-1.45" y1="-1.65" x2="1.45" y2="-1.65" width="0.254" layer="51"/>
<wire x1="1.45" y1="-1.65" x2="1.45" y2="1.75" width="0.254" layer="51"/>
<wire x1="1.45" y1="1.75" x2="-1.45" y2="1.75" width="0.254" layer="51"/>
<wire x1="-1.45" y1="-0.4" x2="-1.45" y2="1.75" width="0.254" layer="21"/>
<wire x1="-1.45" y1="1.75" x2="-0.85" y2="1.75" width="0.254" layer="21"/>
<wire x1="1.45" y1="-0.4" x2="1.45" y2="1.75" width="0.254" layer="21"/>
<wire x1="1.45" y1="1.75" x2="0.85" y2="1.75" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.15" width="0.1016" layer="51"/>
<smd name="2" x="0" y="1.5" dx="1.5" dy="1.6" layer="1"/>
<smd name="1" x="-1" y="-1.825" dx="1.2" dy="1.2" layer="1"/>
<smd name="3" x="1" y="-1.825" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.905" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.175" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.15" y1="-0.15" x2="1.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-1.15" x2="0.15" y2="1.15" layer="51"/>
</package>
<package name="TRIM-3386">
<pad name="2" x="0" y="-2.8575" drill="0.9" diameter="1.778"/>
<pad name="1" x="-2.54" y="-2.8575" drill="0.9" diameter="1.778"/>
<pad name="3" x="2.54" y="-2.8575" drill="0.9" diameter="1.778"/>
<wire x1="-4.7625" y1="-4.7625" x2="4.7625" y2="-4.7625" width="0.127" layer="21"/>
<wire x1="4.7625" y1="-4.7625" x2="4.7625" y2="4.7625" width="0.127" layer="21"/>
<wire x1="4.7625" y1="4.7625" x2="-4.7625" y2="4.7625" width="0.127" layer="21"/>
<wire x1="-4.7625" y1="4.7625" x2="-4.7625" y2="-4.7625" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.302" width="0.127" layer="21"/>
<text x="-5.08" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.35" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="0805-THM">
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-4.5" y1="0" x2="-1.1" y2="0" width="0.3048" layer="1"/>
<wire x1="1" y1="0" x2="5.1" y2="0" width="0.3048" layer="1"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.1306" y="-2.775" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="21"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="21"/>
<rectangle x1="-1.6254" y1="-0.7" x2="-0.4254" y2="0.7" layer="1"/>
<rectangle x1="0.4254" y1="-0.7" x2="1.6254" y2="0.7" layer="1"/>
<rectangle x1="-1.7254" y1="-0.8" x2="-0.3254" y2="0.8" layer="29"/>
<rectangle x1="0.3254" y1="-0.8" x2="1.7254" y2="0.8" layer="29"/>
<rectangle x1="-1.6254" y1="-0.7" x2="-0.4254" y2="0.7" layer="31"/>
<rectangle x1="0.4254" y1="-0.7" x2="1.6254" y2="0.7" layer="31"/>
</package>
<package name="EVQ-Q2">
<wire x1="-3.3" y1="3" x2="3.3" y2="3" width="0.127" layer="21"/>
<wire x1="3.3" y1="3" x2="3.3" y2="-3" width="0.127" layer="21"/>
<wire x1="3.3" y1="-3" x2="-3.3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-3" x2="-3.3" y2="3" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.5033" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1" width="0.127" layer="21"/>
<smd name="B" x="-3.4" y="2" dx="3.2" dy="1.2" layer="1"/>
<smd name="B'" x="3.4" y="2" dx="3.2" dy="1.2" layer="1"/>
<smd name="A'" x="3.4" y="-2" dx="3.2" dy="1.2" layer="1"/>
<smd name="A" x="-3.4" y="-2" dx="3.2" dy="1.2" layer="1"/>
<text x="-3" y="3.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-4.8" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="KMR2">
<wire x1="-2.1" y1="1.4" x2="2.1" y2="1.4" width="0.127" layer="21"/>
<wire x1="2.1" y1="1.4" x2="2.1" y2="-1.4" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1.4" x2="-2.1" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.4" x2="-2.1" y2="1.4" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-0.8" x2="-1.1" y2="-0.2" width="0.127" layer="21" curve="-90"/>
<wire x1="-1.1" y1="-0.2" x2="-1.1" y2="0.2" width="0.127" layer="21"/>
<wire x1="-1.1" y1="0.2" x2="-0.5" y2="0.8" width="0.127" layer="21" curve="-90"/>
<wire x1="-0.5" y1="0.8" x2="0.5" y2="0.8" width="0.127" layer="21"/>
<wire x1="0.5" y1="0.8" x2="1.1" y2="0.2" width="0.127" layer="21" curve="-90"/>
<wire x1="1.1" y1="0.2" x2="1.1" y2="-0.2" width="0.127" layer="21"/>
<wire x1="1.1" y1="-0.2" x2="0.5" y2="-0.8" width="0.127" layer="21" curve="-90"/>
<wire x1="0.5" y1="-0.8" x2="-0.5" y2="-0.8" width="0.127" layer="21"/>
<smd name="1" x="2" y="0.8" dx="1" dy="1" layer="1"/>
<smd name="2" x="2" y="-0.8" dx="1" dy="1" layer="1"/>
<smd name="4" x="-2" y="-0.8" dx="1" dy="1" layer="1"/>
<smd name="3" x="-2" y="0.8" dx="1" dy="1" layer="1"/>
</package>
<package name="CR1220-SMD">
<wire x1="-9.84" y1="-1.64" x2="-6.66" y2="-1.64" width="0.127" layer="21"/>
<wire x1="-6.66" y1="-1.64" x2="-6.66" y2="-3.672" width="0.127" layer="21"/>
<wire x1="-6.66" y1="-3.672" x2="-3.826" y2="-6.66" width="0.127" layer="21"/>
<wire x1="-3.826" y1="-6.66" x2="3.98" y2="-6.66" width="0.127" layer="21"/>
<wire x1="3.98" y1="-6.66" x2="6.66" y2="-3.826" width="0.127" layer="21"/>
<wire x1="6.66" y1="-3.826" x2="6.66" y2="-1.54" width="0.127" layer="21"/>
<wire x1="6.66" y1="-1.54" x2="9.84" y2="-1.54" width="0.127" layer="21"/>
<wire x1="9.84" y1="-1.54" x2="9.84" y2="1.64" width="0.127" layer="21"/>
<wire x1="9.84" y1="1.64" x2="6.66" y2="1.64" width="0.127" layer="21"/>
<wire x1="6.66" y1="1.64" x2="6.66" y2="6.652" width="0.127" layer="21"/>
<wire x1="6.66" y1="6.652" x2="5.842" y2="6.652" width="0.127" layer="21"/>
<wire x1="5.842" y1="6.652" x2="3.702" y2="4.404" width="0.127" layer="21"/>
<wire x1="3.702" y1="4.404" x2="-3.048" y2="4.404" width="0.127" layer="21"/>
<wire x1="-3.048" y1="4.404" x2="-5.334" y2="6.652" width="0.127" layer="21"/>
<wire x1="-5.334" y1="6.652" x2="-6.66" y2="6.652" width="0.127" layer="21"/>
<wire x1="-6.66" y1="6.652" x2="-6.66" y2="1.64" width="0.127" layer="21"/>
<wire x1="-6.66" y1="1.64" x2="-9.84" y2="1.64" width="0.127" layer="21"/>
<wire x1="-9.84" y1="1.64" x2="-9.84" y2="-1.64" width="0.127" layer="21"/>
<circle x="-8.254" y="0" radius="0.9158" width="0.127" layer="21"/>
<circle x="8.4" y="0.154" radius="0.9158" width="0.127" layer="21"/>
<smd name="-" x="0" y="0" dx="3.9" dy="3.9" layer="1"/>
<smd name="+$1" x="-8.2" y="0" dx="3.2" dy="3.2" layer="1"/>
<smd name="+$2" x="8.2" y="0" dx="3.2" dy="3.2" layer="1"/>
<text x="-3.556" y="5.602" size="1.6764" layer="25" font="vector">&gt;NAME</text>
<text x="-4.318" y="-4.55" size="1.6764" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="CR1220-THM">
<wire x1="-7.554" y1="-1.64" x2="-6.66" y2="-1.64" width="0.127" layer="21"/>
<wire x1="-6.66" y1="-1.64" x2="-6.66" y2="-3.672" width="0.127" layer="21"/>
<wire x1="-6.66" y1="-3.672" x2="-3.826" y2="-6.66" width="0.127" layer="21"/>
<wire x1="-3.826" y1="-6.66" x2="3.98" y2="-6.66" width="0.127" layer="21"/>
<wire x1="3.98" y1="-6.66" x2="6.66" y2="-3.826" width="0.127" layer="21"/>
<wire x1="6.66" y1="-3.826" x2="6.66" y2="-1.54" width="0.127" layer="21"/>
<wire x1="6.66" y1="-1.54" x2="7.554" y2="-1.54" width="0.127" layer="21"/>
<wire x1="7.554" y1="-1.54" x2="7.554" y2="1.64" width="0.127" layer="21"/>
<wire x1="7.554" y1="1.64" x2="6.66" y2="1.64" width="0.127" layer="21"/>
<wire x1="6.66" y1="1.64" x2="6.66" y2="5.636" width="0.127" layer="21"/>
<wire x1="6.66" y1="5.636" x2="4.826" y2="5.636" width="0.127" layer="21"/>
<wire x1="4.826" y1="5.636" x2="2.94" y2="3.388" width="0.127" layer="21"/>
<wire x1="2.94" y1="3.388" x2="-3.048" y2="3.388" width="0.127" layer="21"/>
<wire x1="-3.048" y1="3.388" x2="-5.334" y2="5.636" width="0.127" layer="21"/>
<wire x1="-5.334" y1="5.636" x2="-6.66" y2="5.636" width="0.127" layer="21"/>
<wire x1="-6.66" y1="5.636" x2="-6.66" y2="1.64" width="0.127" layer="21"/>
<wire x1="-6.66" y1="1.64" x2="-7.554" y2="1.64" width="0.127" layer="21"/>
<wire x1="-7.554" y1="1.64" x2="-7.554" y2="-1.64" width="0.127" layer="21"/>
<pad name="-" x="0" y="0" drill="0.8" diameter="3.9624" shape="square"/>
<pad name="+1" x="-6.604" y="0" drill="2" diameter="3.175"/>
<pad name="+2" x="6.604" y="0" drill="2" diameter="3.175"/>
<text x="-3.556" y="5.602" size="1.6764" layer="25" font="vector">&gt;NAME</text>
<text x="-4.318" y="-4.55" size="1.6764" layer="27" font="vector">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="DS3231">
<wire x1="-10.16" y1="12.7" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="12.7" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="12.7" width="0.254" layer="94"/>
<text x="-10.16" y="-12.7" size="1.778" layer="96" font="vector">DS3231</text>
<text x="10.16" y="15.24" size="1.778" layer="95" font="vector" rot="R180">&gt;NAME</text>
<pin name="SCL" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="SDA" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="/RST" x="-15.24" y="2.54" length="middle" direction="pas" function="dot"/>
<pin name="VBAT" x="15.24" y="5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="32KHZ" x="-15.24" y="10.16" length="middle" direction="out"/>
<pin name="SQW/INT" x="-15.24" y="5.08" length="middle" direction="out"/>
<pin name="VCC" x="-15.24" y="7.62" length="middle" direction="pwr"/>
<pin name="GND" x="15.24" y="2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="NC" x="-15.24" y="0" length="middle" direction="nc"/>
<pin name="NC1" x="-15.24" y="-2.54" length="middle" direction="nc"/>
<pin name="NC2" x="-15.24" y="-5.08" length="middle" direction="nc"/>
<pin name="NC3" x="-15.24" y="-7.62" length="middle" direction="nc"/>
<pin name="NC4" x="15.24" y="-7.62" length="middle" direction="nc" rot="R180"/>
<pin name="NC5" x="15.24" y="-5.08" length="middle" direction="nc" rot="R180"/>
<pin name="NC6" x="15.24" y="-2.54" length="middle" direction="nc" rot="R180"/>
<pin name="NC7" x="15.24" y="0" length="middle" direction="nc" rot="R180"/>
</symbol>
<symbol name="CLCD">
<wire x1="25.4" y1="12.7" x2="-25.4" y2="12.7" width="0.254" layer="94"/>
<wire x1="-25.4" y1="12.7" x2="-25.4" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-10.16" x2="25.4" y2="-10.16" width="0.254" layer="94"/>
<wire x1="25.4" y1="-10.16" x2="25.4" y2="12.7" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-0.46" x2="-17.32" y2="2.54" width="0.254" layer="94" curve="-90"/>
<wire x1="-17.32" y1="2.54" x2="17.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="17.32" y1="2.54" x2="20.32" y2="-0.46" width="0.254" layer="94" curve="-90"/>
<wire x1="20.32" y1="-0.46" x2="20.32" y2="-4.62" width="0.254" layer="94"/>
<wire x1="20.32" y1="-4.62" x2="17.32" y2="-7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="17.32" y1="-7.62" x2="-17.32" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-17.32" y1="-7.62" x2="-20.32" y2="-4.62" width="0.254" layer="94" curve="-90"/>
<wire x1="-20.32" y1="-4.62" x2="-20.32" y2="-0.46" width="0.254" layer="94"/>
<text x="-17.78" y="-2.54" size="2.1844" layer="94" font="vector" ratio="24">HELLO WORLD!</text>
<text x="-17.78" y="-5.08" size="2.1844" layer="94" font="vector" ratio="24">HD44780 LCD </text>
<text x="-25.4" y="-12.7" size="2.1844" layer="95" font="vector">&gt;NAME</text>
<text x="-10.16" y="-12.7" size="2.1844" layer="96" font="vector">&gt;VALUE</text>
<pin name="K" x="15.24" y="17.78" length="middle" direction="in" rot="R270"/>
<pin name="A" x="12.7" y="17.78" length="middle" direction="in" rot="R270"/>
<pin name="DB7" x="10.16" y="17.78" length="middle" rot="R270"/>
<pin name="DB6" x="7.62" y="17.78" length="middle" rot="R270"/>
<pin name="DB5" x="5.08" y="17.78" length="middle" rot="R270"/>
<pin name="DB4" x="2.54" y="17.78" length="middle" rot="R270"/>
<pin name="DB3" x="0" y="17.78" length="middle" rot="R270"/>
<pin name="DB2" x="-2.54" y="17.78" length="middle" rot="R270"/>
<pin name="DB1" x="-5.08" y="17.78" length="middle" rot="R270"/>
<pin name="DB0" x="-7.62" y="17.78" length="middle" rot="R270"/>
<pin name="E" x="-10.16" y="17.78" length="middle" direction="in" rot="R270"/>
<pin name="RW" x="-12.7" y="17.78" length="middle" direction="in" rot="R270"/>
<pin name="RS" x="-15.24" y="17.78" length="middle" direction="in" rot="R270"/>
<pin name="VO" x="-17.78" y="17.78" length="middle" direction="in" rot="R270"/>
<pin name="VDD" x="-20.32" y="17.78" length="middle" direction="sup" rot="R270"/>
<pin name="VSS" x="-22.86" y="17.78" length="middle" direction="sup" rot="R270"/>
</symbol>
<symbol name="TRIMPOT">
<wire x1="-0.762" y1="2.54" x2="-0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="1.651" y1="0" x2="-1.8796" y2="1.7526" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.54" x2="-0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-2.54" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.508" x2="-3.048" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.508" x2="-2.032" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-2.1597" y1="1.2939" x2="-1.7018" y2="2.2352" width="0.1524" layer="94"/>
<text x="-5.969" y="-3.81" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="A" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="E" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="S" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="TS2">
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="S1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P1" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
<symbol name="3V">
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.4064" layer="94"/>
<wire x1="0.635" y1="2.54" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="0.635" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<text x="-1.27" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.27" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+" x="5.08" y="2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="-" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="+1" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DS3231" prefix="IC">
<gates>
<gate name="G$1" symbol="DS3231" x="0" y="0"/>
</gates>
<devices>
<device name="/SO" package="SO16W">
<connects>
<connect gate="G$1" pin="/RST" pad="4"/>
<connect gate="G$1" pin="32KHZ" pad="1"/>
<connect gate="G$1" pin="GND" pad="13"/>
<connect gate="G$1" pin="NC" pad="5"/>
<connect gate="G$1" pin="NC1" pad="6"/>
<connect gate="G$1" pin="NC2" pad="7"/>
<connect gate="G$1" pin="NC3" pad="8"/>
<connect gate="G$1" pin="NC4" pad="9"/>
<connect gate="G$1" pin="NC5" pad="10"/>
<connect gate="G$1" pin="NC6" pad="11"/>
<connect gate="G$1" pin="NC7" pad="12"/>
<connect gate="G$1" pin="SCL" pad="16"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="SQW/INT" pad="3"/>
<connect gate="G$1" pin="VBAT" pad="14"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HD44780LCD" prefix="X">
<description>&lt;b&gt;HD44780 LCD&lt;/b&gt;
&lt;p&gt;
Standard character type LCDs, available in sizes from 8x1 to 40x4!</description>
<gates>
<gate name="G$1" symbol="CLCD" x="2.54" y="2.54"/>
</gates>
<devices>
<device name="-1602" package="LCD1602">
<connects>
<connect gate="G$1" pin="A" pad="15"/>
<connect gate="G$1" pin="DB0" pad="7"/>
<connect gate="G$1" pin="DB1" pad="8"/>
<connect gate="G$1" pin="DB2" pad="9"/>
<connect gate="G$1" pin="DB3" pad="10"/>
<connect gate="G$1" pin="DB4" pad="11"/>
<connect gate="G$1" pin="DB5" pad="12"/>
<connect gate="G$1" pin="DB6" pad="13"/>
<connect gate="G$1" pin="DB7" pad="14"/>
<connect gate="G$1" pin="E" pad="6"/>
<connect gate="G$1" pin="K" pad="16"/>
<connect gate="G$1" pin="RS" pad="4"/>
<connect gate="G$1" pin="RW" pad="5"/>
<connect gate="G$1" pin="VDD" pad="2"/>
<connect gate="G$1" pin="VO" pad="3"/>
<connect gate="G$1" pin="VSS" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRIMPOT" prefix="TM" uservalue="yes">
<description>SMT trimmer potentiometer part number TC33X
&lt;p&gt;http://www.ladyada.net/library/eagle&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="TRIMPOT" x="0" y="0"/>
</gates>
<devices>
<device name="TC33X" package="TC33X">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="E" pad="3"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3386" package="TRIM-3386">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="E" pad="3"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FLIPFLOP-RES" prefix="R" uservalue="yes">
<description>&lt;B&gt;Flip Flop resistor&lt;/b&gt;
&lt;p&gt;
An 0805 resistor inside a standard 1/4W 5% resistor. Makes for easy mods</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0805-THM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SPST_TACT" prefix="SW">
<description>SMT 6mm switch, EVQQ2 series
&lt;p&gt;http://www.ladyada.net/library/eagle&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="TS2" x="0" y="0"/>
</gates>
<devices>
<device name="-EVQQ2" package="EVQ-Q2">
<connects>
<connect gate="G$1" pin="P" pad="A"/>
<connect gate="G$1" pin="P1" pad="A'"/>
<connect gate="G$1" pin="S" pad="B"/>
<connect gate="G$1" pin="S1" pad="B'"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-KMR2" package="KMR2">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
<connect gate="G$1" pin="P1" pad="3"/>
<connect gate="G$1" pin="S" pad="2"/>
<connect gate="G$1" pin="S1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CR1220" uservalue="yes">
<description>&lt;b&gt;CR1216/CR1220/CR1225 12mm 3V lithium coin cell &lt;/b&gt;
&lt;p&gt;
Great for battery packup. Both SMT and THM holders. 
&lt;br&gt;
Note the THM package has the ground pads on both sides of the PCB so that the masks are the same - this reduces the cost</description>
<gates>
<gate name="G$1" symbol="3V" x="0" y="0"/>
</gates>
<devices>
<device name="SMT" package="CR1220-SMD">
<connects>
<connect gate="G$1" pin="+" pad="+$1"/>
<connect gate="G$1" pin="+1" pad="+$2"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="THM" package="CR1220-THM">
<connects>
<connect gate="G$1" pin="+" pad="+1"/>
<connect gate="G$1" pin="+1" pad="+2"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="MySMD" deviceset="ATMEGA168-20AU" device="" value="ATMEGA328P"/>
<part name="FRAME1" library="frames" deviceset="FRAME_B_L" device=""/>
<part name="U$1" library="MyPOW" deviceset="COM" device=""/>
<part name="U$2" library="MyPOW" deviceset="COM" device=""/>
<part name="U$3" library="MyPOW" deviceset="COM" device=""/>
<part name="U$5" library="MyPOW" deviceset="COM" device=""/>
<part name="U$6" library="MyPOW" deviceset="+5V" device=""/>
<part name="U$7" library="MyPOW" deviceset="VIN" device=""/>
<part name="U$8" library="MyPOW" deviceset="3V3" device=""/>
<part name="U$9" library="MyPOW" deviceset="+5V" device=""/>
<part name="C1" library="MySMD" deviceset="CAP0805-NP" device="" value="0.1uF"/>
<part name="C3" library="MySMD" deviceset="CAP0805-NP" device="" value="0.1uF"/>
<part name="U$10" library="MyPOW" deviceset="COM" device=""/>
<part name="C4" library="MySMD" deviceset="CAP0805-NP" device="" value="0.1uF"/>
<part name="U$13" library="MyPOW" deviceset="COM" device=""/>
<part name="C2" library="MySMD" deviceset="CAP-TANA-P" device="" value="4.7uF"/>
<part name="C8" library="MySMD" deviceset="CAP-TANA-P" device="" value="4.7uF"/>
<part name="U$16" library="MyPOW" deviceset="COM" device=""/>
<part name="C7" library="MySMD" deviceset="CAP0805-NP" device="" value="0.1uF"/>
<part name="C9" library="MySMD" deviceset="CAP0805-NP" device="" value="0.1uF"/>
<part name="U$17" library="MyPOW" deviceset="COM" device=""/>
<part name="U3" library="MySMD" deviceset="UA78M05" device=""/>
<part name="U$18" library="MyPOW" deviceset="+5V" device=""/>
<part name="U$19" library="MyPOW" deviceset="VIN" device=""/>
<part name="LED1" library="MySMD" deviceset="LED0805" device="" value="RED_RX"/>
<part name="LED2" library="MySMD" deviceset="LED0805" device="" value="GREEN_TX"/>
<part name="LED3" library="MySMD" deviceset="LED0805" device="" value="L_AMBER"/>
<part name="LED4" library="MySMD" deviceset="LED0805" device="" value="BLUE"/>
<part name="D1" library="MySMD" deviceset="MBR0520" device=""/>
<part name="U$15" library="MyPOW" deviceset="+5V" device=""/>
<part name="U$21" library="MyPOW" deviceset="VUSB" device=""/>
<part name="U$23" library="MyPOW" deviceset="VUSB" device=""/>
<part name="U$12" library="MyPOW" deviceset="+5V" device=""/>
<part name="U$24" library="MyPOW" deviceset="COM" device=""/>
<part name="U$26" library="MyPOW" deviceset="3V3" device=""/>
<part name="U$27" library="MyPOW" deviceset="COM" device=""/>
<part name="SW1" library="MySMD" deviceset="PB157" device="" value="RESET"/>
<part name="U$28" library="MyPOW" deviceset="COM" device=""/>
<part name="U$22" library="MyPOW" deviceset="VUSB" device=""/>
<part name="U$29" library="MyPOW" deviceset="COM" device=""/>
<part name="U$30" library="MyPOW" deviceset="COM" device=""/>
<part name="RP1" library="MySMD" deviceset="RES4NT" device="" value="1K"/>
<part name="RP2" library="MySMD" deviceset="RES4NT" device="" value="330"/>
<part name="U$31" library="MyPOW" deviceset="+5V" device=""/>
<part name="U$33" library="MyPOW" deviceset="COM" device=""/>
<part name="U$32" library="MyPOW" deviceset="COM" device=""/>
<part name="U$34" library="MyPOW" deviceset="COM" device=""/>
<part name="U$20" library="MyPOW" deviceset="NC" device=""/>
<part name="U$38" library="MyPOW" deviceset="NC" device=""/>
<part name="U$39" library="MyPOW" deviceset="NC" device=""/>
<part name="U$40" library="MyPOW" deviceset="NC" device=""/>
<part name="U$41" library="MyPOW" deviceset="NC" device=""/>
<part name="U$42" library="MyPOW" deviceset="NC" device=""/>
<part name="U$43" library="MyPOW" deviceset="NC" device=""/>
<part name="U$44" library="MyPOW" deviceset="NC" device=""/>
<part name="U$45" library="MyPOW" deviceset="NC" device=""/>
<part name="U$47" library="MyPOW" deviceset="NC" device=""/>
<part name="U$48" library="MyPOW" deviceset="NC" device=""/>
<part name="U$49" library="MyPOW" deviceset="NC" device=""/>
<part name="U$74" library="MyPOW" deviceset="NC" device=""/>
<part name="U$78" library="MyPOW" deviceset="NC" device=""/>
<part name="U$46" library="MyPOW" deviceset="NC" device=""/>
<part name="U$50" library="MyPOW" deviceset="NC" device=""/>
<part name="U2" library="MySMD2" deviceset="FT232RL" device=""/>
<part name="J3" library="MyCON2" deviceset="USB-MINI-B" device="%C"/>
<part name="Y1" library="MySMD2" deviceset="RESONATOR" device="MU" value="16MHz"/>
<part name="J1" library="MyCON2" deviceset="HEAD15-NOSS" device=""/>
<part name="J2" library="MyCON2" deviceset="HEAD15-NOSS-1" device=""/>
<part name="J4" library="MyCON2" deviceset="HEAD3X2" device=""/>
<part name="U$11" library="MyPOW" deviceset="+5V" device=""/>
<part name="U$14" library="MyPOW" deviceset="+5V" device=""/>
<part name="U$25" library="MyPOW" deviceset="+5V" device=""/>
<part name="U$35" library="MyPOW" deviceset="+5V" device=""/>
<part name="IC1" library="adafruit" deviceset="DS3231" device="/SO"/>
<part name="X1" library="adafruit" deviceset="HD44780LCD" device="-1602"/>
<part name="TM1" library="adafruit" deviceset="TRIMPOT" device="3386"/>
<part name="MENU" library="adafruit" deviceset="SPST_TACT" device="-KMR2" value=""/>
<part name="SET" library="adafruit" deviceset="SPST_TACT" device="-KMR2" value=""/>
<part name="R2" library="adafruit" deviceset="FLIPFLOP-RES" device="" value=" "/>
<part name="R3" library="adafruit" deviceset="FLIPFLOP-RES" device="" value=" "/>
<part name="U$4" library="MyPOW" deviceset="COM" device=""/>
<part name="U$36" library="MyPOW" deviceset="+5V" device=""/>
<part name="U$37" library="adafruit" deviceset="CR1220" device="THM"/>
<part name="U$51" library="MyPOW" deviceset="COM" device=""/>
<part name="U$52" library="MyPOW" deviceset="COM" device=""/>
<part name="U$53" library="MyPOW" deviceset="+5V" device=""/>
<part name="U$54" library="MyPOW" deviceset="COM" device=""/>
<part name="R4" library="adafruit" deviceset="FLIPFLOP-RES" device="" value="220"/>
<part name="U$55" library="MyPOW" deviceset="+5V" device=""/>
<part name="U$56" library="MyPOW" deviceset="+5V" device=""/>
<part name="U$57" library="MyPOW" deviceset="COM" device=""/>
<part name="U$58" library="MyPOW" deviceset="COM" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="53.34" y1="193.04" x2="73.66" y2="193.04" width="0.3048" layer="94"/>
<wire x1="53.34" y1="233.68" x2="58.42" y2="233.68" width="0.3048" layer="94"/>
<wire x1="58.42" y1="233.68" x2="68.58" y2="233.68" width="0.3048" layer="94"/>
<wire x1="68.58" y1="233.68" x2="73.66" y2="233.68" width="0.3048" layer="94"/>
<wire x1="68.58" y1="233.68" x2="58.42" y2="233.68" width="0.3048" layer="94" curve="-180"/>
<text x="172.72" y="165.1" size="3.81" layer="94" ratio="10">Arduino</text>
<text x="45.72" y="68.58" size="3.81" layer="94" ratio="10">+5V REG</text>
<text x="269.24" y="66.04" size="3.81" layer="94" ratio="10">ICSP</text>
<text x="332.74" y="251.46" size="3.81" layer="94" ratio="10">USB</text>
<text x="162.56" y="50.8" size="3.81" layer="94" ratio="10">+5V AUTO SELECTOR</text>
<text x="246.38" y="25.4" size="3.81" layer="94" ratio="10">NOT USED</text>
</plain>
<instances>
<instance part="U1" gate="G$1" x="185.42" y="129.54"/>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="U$1" gate="G$1" x="20.32" y="210.82"/>
<instance part="U$2" gate="G$1" x="106.68" y="210.82"/>
<instance part="U$3" gate="G$1" x="287.02" y="43.18"/>
<instance part="U$5" gate="G$1" x="157.48" y="99.06"/>
<instance part="U$6" gate="G$1" x="109.22" y="238.76"/>
<instance part="U$7" gate="G$1" x="101.6" y="238.76"/>
<instance part="U$8" gate="G$1" x="116.84" y="238.76"/>
<instance part="U$9" gate="G$1" x="210.82" y="198.12"/>
<instance part="C1" gate="G$1" x="137.16" y="132.08"/>
<instance part="C3" gate="G$1" x="233.68" y="190.5"/>
<instance part="U$10" gate="G$1" x="137.16" y="121.92"/>
<instance part="C4" gate="G$1" x="299.72" y="236.22" rot="R90"/>
<instance part="U$13" gate="G$1" x="124.46" y="142.24"/>
<instance part="C2" gate="G$1" x="220.98" y="190.5"/>
<instance part="C8" gate="G$1" x="360.68" y="66.04"/>
<instance part="U$16" gate="G$1" x="233.68" y="177.8"/>
<instance part="C7" gate="G$1" x="363.22" y="190.5"/>
<instance part="C9" gate="G$1" x="370.84" y="66.04"/>
<instance part="U$17" gate="G$1" x="365.76" y="55.88"/>
<instance part="U3" gate="G$1" x="55.88" y="50.8"/>
<instance part="U$18" gate="G$1" x="76.2" y="58.42"/>
<instance part="U$19" gate="G$1" x="35.56" y="58.42"/>
<instance part="LED1" gate="G$1" x="378.46" y="251.46" rot="R270"/>
<instance part="LED2" gate="G$1" x="391.16" y="251.46" rot="R270"/>
<instance part="LED3" gate="G$1" x="231.14" y="81.28" rot="R270"/>
<instance part="LED4" gate="G$1" x="101.6" y="43.18" rot="R270"/>
<instance part="D1" gate="G$1" x="185.42" y="38.1" rot="MR0"/>
<instance part="U$15" gate="G$1" x="177.8" y="43.18"/>
<instance part="U$21" gate="G$1" x="198.12" y="43.18"/>
<instance part="U$23" gate="G$1" x="365.76" y="78.74"/>
<instance part="U$12" gate="G$1" x="287.02" y="63.5"/>
<instance part="U$24" gate="G$1" x="317.5" y="190.5"/>
<instance part="U$26" gate="G$1" x="363.22" y="210.82"/>
<instance part="U$27" gate="G$1" x="363.22" y="180.34"/>
<instance part="SW1" gate="G$1" x="114.3" y="154.94"/>
<instance part="U$28" gate="G$1" x="99.06" y="139.7"/>
<instance part="U$22" gate="G$1" x="386.08" y="210.82"/>
<instance part="U$29" gate="G$1" x="386.08" y="187.96"/>
<instance part="U$30" gate="G$1" x="416.56" y="187.96"/>
<instance part="RP1" gate="A" x="259.08" y="20.32"/>
<instance part="RP1" gate="B" x="246.38" y="172.72"/>
<instance part="RP1" gate="C" x="246.38" y="165.1"/>
<instance part="RP1" gate="D" x="142.24" y="165.1" rot="R90"/>
<instance part="RP2" gate="A" x="231.14" y="99.06" rot="R270"/>
<instance part="RP2" gate="B" x="88.9" y="53.34"/>
<instance part="RP2" gate="C" x="378.46" y="236.22" rot="R90"/>
<instance part="RP2" gate="D" x="391.16" y="236.22" rot="R90"/>
<instance part="U$31" gate="G$1" x="142.24" y="177.8"/>
<instance part="U$33" gate="G$1" x="101.6" y="33.02"/>
<instance part="U$32" gate="G$1" x="55.88" y="33.02"/>
<instance part="U$34" gate="G$1" x="231.14" y="71.12"/>
<instance part="U$20" gate="G$1" x="317.5" y="215.9"/>
<instance part="U$38" gate="G$1" x="317.5" y="218.44"/>
<instance part="U$39" gate="G$1" x="358.14" y="233.68" rot="R180"/>
<instance part="U$40" gate="G$1" x="358.14" y="220.98" rot="R180"/>
<instance part="U$41" gate="G$1" x="358.14" y="228.6" rot="R180"/>
<instance part="U$42" gate="G$1" x="358.14" y="231.14" rot="R180"/>
<instance part="U$43" gate="G$1" x="317.5" y="226.06"/>
<instance part="U$44" gate="G$1" x="317.5" y="223.52"/>
<instance part="U$45" gate="G$1" x="317.5" y="220.98"/>
<instance part="U$47" gate="G$1" x="358.14" y="218.44" rot="R180"/>
<instance part="U$48" gate="G$1" x="358.14" y="215.9" rot="R180"/>
<instance part="U$49" gate="G$1" x="358.14" y="210.82" rot="R180"/>
<instance part="U$74" gate="G$1" x="269.24" y="20.32" rot="R180"/>
<instance part="U$78" gate="G$1" x="248.92" y="20.32"/>
<instance part="U$46" gate="G$1" x="317.5" y="210.82"/>
<instance part="U$50" gate="G$1" x="317.5" y="233.68"/>
<instance part="U2" gate="G$1" x="337.82" y="218.44"/>
<instance part="J3" gate="G$1" x="401.32" y="198.12"/>
<instance part="Y1" gate="G$1" x="134.62" y="147.32"/>
<instance part="J1" gate="G$1" x="50.8" y="213.36" rot="MR0"/>
<instance part="J2" gate="G$1" x="76.2" y="213.36"/>
<instance part="J4" gate="G$1" x="274.32" y="53.34"/>
<instance part="U$11" gate="G$1" x="312.42" y="251.46"/>
<instance part="U$14" gate="G$1" x="360.68" y="251.46"/>
<instance part="U$25" gate="G$1" x="378.46" y="264.16"/>
<instance part="U$35" gate="G$1" x="391.16" y="264.16"/>
<instance part="IC1" gate="G$1" x="81.28" y="109.22"/>
<instance part="X1" gate="G$1" x="292.1" y="165.1" rot="R180"/>
<instance part="TM1" gate="G$1" x="307.34" y="91.44" rot="R90"/>
<instance part="MENU" gate="G$1" x="261.62" y="139.7" smashed="yes" rot="R270">
<attribute name="NAME" x="259.08" y="146.05" size="1.778" layer="95"/>
<attribute name="VALUE" x="264.795" y="143.51" size="1.778" layer="96"/>
</instance>
<instance part="SET" gate="G$1" x="243.84" y="149.86" smashed="yes" rot="R270">
<attribute name="NAME" x="241.3" y="156.21" size="1.778" layer="95"/>
<attribute name="VALUE" x="247.015" y="153.67" size="1.778" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="106.68" y="124.46" rot="R90"/>
<instance part="R3" gate="G$1" x="114.3" y="127" rot="R90"/>
<instance part="U$4" gate="G$1" x="81.28" y="76.2"/>
<instance part="U$36" gate="G$1" x="55.88" y="116.84" rot="R90"/>
<instance part="U$37" gate="G$1" x="124.46" y="109.22"/>
<instance part="U$51" gate="G$1" x="274.32" y="129.54" rot="R270"/>
<instance part="U$52" gate="G$1" x="314.96" y="124.46"/>
<instance part="U$53" gate="G$1" x="320.04" y="119.38" rot="R270"/>
<instance part="U$54" gate="G$1" x="304.8" y="121.92"/>
<instance part="R4" gate="G$1" x="279.4" y="101.6" rot="R90"/>
<instance part="U$55" gate="G$1" x="279.4" y="93.98" rot="R180"/>
<instance part="U$56" gate="G$1" x="114.3" y="137.16"/>
<instance part="U$57" gate="G$1" x="269.24" y="139.7" rot="R90"/>
<instance part="U$58" gate="G$1" x="251.46" y="149.86" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="D5" class="0">
<segment>
<wire x1="25.4" y1="213.36" x2="40.64" y2="213.36" width="0.1524" layer="91"/>
<label x="27.94" y="213.36" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="8"/>
</segment>
<segment>
<label x="210.82" y="132.08" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PD5(T1)"/>
<pinref part="X1" gate="G$1" pin="DB5"/>
<wire x1="287.02" y1="147.32" x2="287.02" y2="119.38" width="0.1524" layer="91"/>
<wire x1="287.02" y1="119.38" x2="233.68" y2="119.38" width="0.1524" layer="91"/>
<wire x1="233.68" y1="119.38" x2="233.68" y2="132.08" width="0.1524" layer="91"/>
<wire x1="233.68" y1="132.08" x2="205.74" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D4" class="0">
<segment>
<wire x1="25.4" y1="215.9" x2="40.64" y2="215.9" width="0.1524" layer="91"/>
<label x="27.94" y="215.9" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="7"/>
</segment>
<segment>
<label x="210.82" y="134.62" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PD4(T0)"/>
<wire x1="205.74" y1="134.62" x2="236.22" y2="134.62" width="0.1524" layer="91"/>
<wire x1="236.22" y1="134.62" x2="236.22" y2="121.92" width="0.1524" layer="91"/>
<wire x1="236.22" y1="121.92" x2="284.48" y2="121.92" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="DB6"/>
<wire x1="284.48" y1="121.92" x2="284.48" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D3" class="0">
<segment>
<wire x1="25.4" y1="218.44" x2="40.64" y2="218.44" width="0.1524" layer="91"/>
<label x="27.94" y="218.44" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="6"/>
</segment>
<segment>
<label x="210.82" y="137.16" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PD3(INT1)"/>
<pinref part="X1" gate="G$1" pin="DB7"/>
<wire x1="281.94" y1="147.32" x2="281.94" y2="124.46" width="0.1524" layer="91"/>
<wire x1="281.94" y1="124.46" x2="238.76" y2="124.46" width="0.1524" layer="91"/>
<wire x1="238.76" y1="124.46" x2="238.76" y2="137.16" width="0.1524" layer="91"/>
<wire x1="238.76" y1="137.16" x2="205.74" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D2" class="0">
<segment>
<wire x1="25.4" y1="220.98" x2="40.64" y2="220.98" width="0.1524" layer="91"/>
<label x="27.94" y="220.98" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="5"/>
</segment>
<segment>
<wire x1="223.52" y1="139.7" x2="205.74" y2="139.7" width="0.1524" layer="91"/>
<label x="210.82" y="139.7" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PD2(IND0)"/>
<wire x1="223.52" y1="139.7" x2="223.52" y2="129.54" width="0.1524" layer="91"/>
<wire x1="223.52" y1="129.54" x2="246.38" y2="129.54" width="0.1524" layer="91"/>
<wire x1="246.38" y1="129.54" x2="246.38" y2="66.04" width="0.1524" layer="91"/>
<wire x1="246.38" y1="66.04" x2="109.22" y2="66.04" width="0.1524" layer="91"/>
<wire x1="109.22" y1="66.04" x2="109.22" y2="93.98" width="0.1524" layer="91"/>
<wire x1="109.22" y1="93.98" x2="60.96" y2="93.98" width="0.1524" layer="91"/>
<wire x1="60.96" y1="93.98" x2="60.96" y2="114.3" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="SQW/INT"/>
<wire x1="60.96" y1="114.3" x2="66.04" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<wire x1="25.4" y1="226.06" x2="40.64" y2="226.06" width="0.1524" layer="91"/>
<label x="27.94" y="226.06" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="86.36" y1="226.06" x2="101.6" y2="226.06" width="0.1524" layer="91"/>
<label x="88.9" y="226.06" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="246.38" y1="48.26" x2="264.16" y2="48.26" width="0.1524" layer="91"/>
<label x="248.92" y="48.26" size="1.778" layer="95"/>
<pinref part="J4" gate="G$1" pin="5"/>
</segment>
<segment>
<wire x1="284.48" y1="236.22" x2="294.64" y2="236.22" width="0.1524" layer="91"/>
<label x="287.02" y="236.22" size="1.778" layer="95"/>
<pinref part="C4" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="121.92" y1="154.94" x2="142.24" y2="154.94" width="0.1524" layer="91"/>
<wire x1="142.24" y1="154.94" x2="162.56" y2="154.94" width="0.1524" layer="91"/>
<wire x1="142.24" y1="157.48" x2="142.24" y2="154.94" width="0.1524" layer="91"/>
<junction x="142.24" y="154.94"/>
<label x="127" y="154.94" size="1.778" layer="95"/>
<pinref part="SW1" gate="G$1" pin="2"/>
<pinref part="U1" gate="G$1" pin="PC6(RESET)"/>
<pinref part="RP1" gate="D" pin="1"/>
</segment>
</net>
<net name="D1/TX" class="0">
<segment>
<wire x1="25.4" y1="231.14" x2="40.64" y2="231.14" width="0.1524" layer="91"/>
<label x="27.94" y="231.14" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="238.76" y1="167.64" x2="205.74" y2="142.24" width="0.1524" layer="91"/>
<label x="210.82" y="142.24" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PD1(TXD)"/>
<pinref part="RP1" gate="C" pin="1"/>
<wire x1="238.76" y1="167.64" x2="238.76" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D6" class="0">
<segment>
<wire x1="25.4" y1="210.82" x2="40.64" y2="210.82" width="0.1524" layer="91"/>
<label x="27.94" y="210.82" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="9"/>
</segment>
<segment>
<wire x1="220.98" y1="129.54" x2="205.74" y2="129.54" width="0.1524" layer="91"/>
<label x="210.82" y="129.54" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PD6(AIN0)"/>
<pinref part="X1" gate="G$1" pin="DB4"/>
<wire x1="289.56" y1="147.32" x2="289.56" y2="116.84" width="0.1524" layer="91"/>
<wire x1="289.56" y1="116.84" x2="231.14" y2="116.84" width="0.1524" layer="91"/>
<wire x1="231.14" y1="116.84" x2="231.14" y2="127" width="0.1524" layer="91"/>
<wire x1="231.14" y1="127" x2="220.98" y2="127" width="0.1524" layer="91"/>
<wire x1="220.98" y1="127" x2="220.98" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D7" class="0">
<segment>
<wire x1="40.64" y1="208.28" x2="25.4" y2="208.28" width="0.1524" layer="91"/>
<label x="27.94" y="208.28" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="10"/>
</segment>
<segment>
<wire x1="218.44" y1="127" x2="205.74" y2="127" width="0.1524" layer="91"/>
<label x="210.82" y="127" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PD7(AIN1)"/>
<wire x1="218.44" y1="127" x2="218.44" y2="142.24" width="0.1524" layer="91"/>
<wire x1="218.44" y1="142.24" x2="251.46" y2="142.24" width="0.1524" layer="91"/>
<wire x1="251.46" y1="142.24" x2="251.46" y2="139.7" width="0.1524" layer="91"/>
<pinref part="MENU" gate="G$1" pin="P"/>
<wire x1="251.46" y1="139.7" x2="256.54" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D8" class="0">
<segment>
<wire x1="25.4" y1="205.74" x2="40.64" y2="205.74" width="0.1524" layer="91"/>
<label x="27.94" y="205.74" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="11"/>
</segment>
<segment>
<wire x1="228.6" y1="121.92" x2="205.74" y2="121.92" width="0.1524" layer="91"/>
<label x="210.82" y="121.92" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PB0(ICP)"/>
<pinref part="SET" gate="G$1" pin="P"/>
<wire x1="228.6" y1="121.92" x2="228.6" y2="149.86" width="0.1524" layer="91"/>
<wire x1="228.6" y1="149.86" x2="238.76" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D9" class="0">
<segment>
<wire x1="25.4" y1="203.2" x2="40.64" y2="203.2" width="0.1524" layer="91"/>
<label x="27.94" y="203.2" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="12"/>
</segment>
<segment>
<wire x1="228.6" y1="119.38" x2="205.74" y2="119.38" width="0.1524" layer="91"/>
<label x="210.82" y="119.38" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PB1(OC1)"/>
</segment>
</net>
<net name="D10" class="0">
<segment>
<wire x1="25.4" y1="200.66" x2="40.64" y2="200.66" width="0.1524" layer="91"/>
<label x="27.94" y="200.66" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="13"/>
</segment>
<segment>
<wire x1="228.6" y1="116.84" x2="205.74" y2="116.84" width="0.1524" layer="91"/>
<label x="210.82" y="116.84" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PB2(SS)"/>
</segment>
</net>
<net name="D11/MOSI" class="0">
<segment>
<wire x1="25.4" y1="198.12" x2="40.64" y2="198.12" width="0.1524" layer="91"/>
<label x="27.94" y="198.12" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="14"/>
</segment>
<segment>
<label x="210.82" y="114.3" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PB3(MOSI)"/>
<wire x1="302.26" y1="114.3" x2="205.74" y2="114.3" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="E"/>
<wire x1="302.26" y1="114.3" x2="302.26" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="302.26" y1="53.34" x2="284.48" y2="53.34" width="0.1524" layer="91"/>
<label x="287.02" y="53.34" size="1.778" layer="95"/>
<pinref part="J4" gate="G$1" pin="4"/>
</segment>
</net>
<net name="D12/MISO" class="0">
<segment>
<wire x1="25.4" y1="195.58" x2="40.64" y2="195.58" width="0.1524" layer="91"/>
<label x="27.94" y="195.58" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="15"/>
</segment>
<segment>
<label x="210.82" y="111.76" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PB4(MISO)"/>
<pinref part="X1" gate="G$1" pin="RS"/>
<wire x1="205.74" y1="111.76" x2="307.34" y2="111.76" width="0.1524" layer="91"/>
<wire x1="307.34" y1="111.76" x2="307.34" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="246.38" y1="58.42" x2="264.16" y2="58.42" width="0.1524" layer="91"/>
<label x="248.92" y="58.42" size="1.778" layer="95"/>
<pinref part="J4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<wire x1="149.86" y1="127" x2="162.56" y2="127" width="0.1524" layer="91"/>
<label x="152.4" y="127" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PC3(ADC3)"/>
</segment>
<segment>
<wire x1="86.36" y1="210.82" x2="101.6" y2="210.82" width="0.1524" layer="91"/>
<label x="88.9" y="210.82" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="9"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<wire x1="149.86" y1="129.54" x2="162.56" y2="129.54" width="0.1524" layer="91"/>
<label x="152.4" y="129.54" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PC2(ADC2)"/>
</segment>
<segment>
<wire x1="101.6" y1="208.28" x2="86.36" y2="208.28" width="0.1524" layer="91"/>
<label x="88.9" y="208.28" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="10"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<wire x1="149.86" y1="132.08" x2="162.56" y2="132.08" width="0.1524" layer="91"/>
<label x="152.4" y="132.08" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PC1(ADC1)"/>
</segment>
<segment>
<wire x1="86.36" y1="205.74" x2="101.6" y2="205.74" width="0.1524" layer="91"/>
<label x="88.9" y="205.74" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="11"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<wire x1="149.86" y1="134.62" x2="162.56" y2="134.62" width="0.1524" layer="91"/>
<label x="152.4" y="134.62" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PC0(ADC0)"/>
</segment>
<segment>
<wire x1="86.36" y1="203.2" x2="101.6" y2="203.2" width="0.1524" layer="91"/>
<label x="88.9" y="203.2" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="12"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<wire x1="144.78" y1="124.46" x2="162.56" y2="124.46" width="0.1524" layer="91"/>
<label x="152.4" y="124.46" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PC4(ADC4)"/>
<pinref part="IC1" gate="G$1" pin="SDA"/>
<wire x1="96.52" y1="116.84" x2="114.3" y2="116.84" width="0.1524" layer="91"/>
<wire x1="114.3" y1="116.84" x2="144.78" y2="116.84" width="0.1524" layer="91"/>
<wire x1="144.78" y1="116.84" x2="144.78" y2="124.46" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="114.3" y1="116.84" x2="114.3" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="86.36" y1="213.36" x2="101.6" y2="213.36" width="0.1524" layer="91"/>
<label x="88.9" y="213.36" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="8"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<wire x1="142.24" y1="121.92" x2="162.56" y2="121.92" width="0.1524" layer="91"/>
<label x="152.4" y="121.92" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PC5(ADC5)"/>
<pinref part="IC1" gate="G$1" pin="SCL"/>
<wire x1="96.52" y1="119.38" x2="106.68" y2="119.38" width="0.1524" layer="91"/>
<wire x1="106.68" y1="119.38" x2="142.24" y2="119.38" width="0.1524" layer="91"/>
<wire x1="142.24" y1="119.38" x2="142.24" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<junction x="106.68" y="119.38"/>
</segment>
<segment>
<wire x1="86.36" y1="215.9" x2="101.6" y2="215.9" width="0.1524" layer="91"/>
<label x="88.9" y="215.9" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="7"/>
</segment>
</net>
<net name="A6" class="0">
<segment>
<wire x1="149.86" y1="119.38" x2="162.56" y2="119.38" width="0.1524" layer="91"/>
<label x="152.4" y="119.38" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="ADC6"/>
</segment>
<segment>
<wire x1="86.36" y1="218.44" x2="101.6" y2="218.44" width="0.1524" layer="91"/>
<label x="88.9" y="218.44" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="6"/>
</segment>
</net>
<net name="A7" class="0">
<segment>
<wire x1="149.86" y1="116.84" x2="162.56" y2="116.84" width="0.1524" layer="91"/>
<label x="152.4" y="116.84" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="ADC7"/>
</segment>
<segment>
<wire x1="86.36" y1="220.98" x2="101.6" y2="220.98" width="0.1524" layer="91"/>
<label x="88.9" y="220.98" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="5"/>
</segment>
</net>
<net name="AREF" class="0">
<segment>
<wire x1="86.36" y1="200.66" x2="101.6" y2="200.66" width="0.1524" layer="91"/>
<label x="88.9" y="200.66" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="13"/>
</segment>
<segment>
<wire x1="162.56" y1="139.7" x2="137.16" y2="139.7" width="0.1524" layer="91"/>
<wire x1="137.16" y1="139.7" x2="137.16" y2="137.16" width="0.1524" layer="91"/>
<label x="142.24" y="139.7" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="AREF"/>
<pinref part="C1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="D13/SCK" class="0">
<segment>
<wire x1="86.36" y1="195.58" x2="101.6" y2="195.58" width="0.1524" layer="91"/>
<label x="88.9" y="195.58" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="15"/>
</segment>
<segment>
<wire x1="231.14" y1="109.22" x2="205.74" y2="109.22" width="0.1524" layer="91"/>
<wire x1="231.14" y1="106.68" x2="231.14" y2="109.22" width="0.1524" layer="91"/>
<label x="210.82" y="109.22" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PB5(SCK)"/>
<pinref part="RP2" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="246.38" y1="53.34" x2="264.16" y2="53.34" width="0.1524" layer="91"/>
<label x="248.92" y="53.34" size="1.778" layer="95"/>
<pinref part="J4" gate="G$1" pin="3"/>
</segment>
</net>
<net name="COM" class="0">
<segment>
<wire x1="20.32" y1="223.52" x2="40.64" y2="223.52" width="0.1524" layer="91"/>
<wire x1="20.32" y1="223.52" x2="20.32" y2="213.36" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="COM"/>
<pinref part="J1" gate="G$1" pin="4"/>
</segment>
<segment>
<wire x1="86.36" y1="228.6" x2="106.68" y2="228.6" width="0.1524" layer="91"/>
<wire x1="106.68" y1="228.6" x2="106.68" y2="213.36" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="COM"/>
<pinref part="J2" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="137.16" y1="127" x2="137.16" y2="124.46" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="U$10" gate="G$1" pin="COM"/>
</segment>
<segment>
<wire x1="162.56" y1="111.76" x2="157.48" y2="111.76" width="0.1524" layer="91"/>
<wire x1="157.48" y1="111.76" x2="157.48" y2="109.22" width="0.1524" layer="91"/>
<wire x1="157.48" y1="109.22" x2="157.48" y2="106.68" width="0.1524" layer="91"/>
<wire x1="162.56" y1="109.22" x2="157.48" y2="109.22" width="0.1524" layer="91"/>
<wire x1="162.56" y1="106.68" x2="157.48" y2="106.68" width="0.1524" layer="91"/>
<wire x1="157.48" y1="101.6" x2="157.48" y2="106.68" width="0.1524" layer="91"/>
<junction x="157.48" y="109.22"/>
<junction x="157.48" y="106.68"/>
<pinref part="U1" gate="G$1" pin="AGND"/>
<pinref part="U1" gate="G$1" pin="GND1"/>
<pinref part="U1" gate="G$1" pin="GND2"/>
<pinref part="U$5" gate="G$1" pin="COM"/>
</segment>
<segment>
<wire x1="233.68" y1="185.42" x2="233.68" y2="182.88" width="0.1524" layer="91"/>
<wire x1="233.68" y1="182.88" x2="233.68" y2="180.34" width="0.1524" layer="91"/>
<wire x1="220.98" y1="185.42" x2="220.98" y2="182.88" width="0.1524" layer="91"/>
<wire x1="220.98" y1="182.88" x2="233.68" y2="182.88" width="0.1524" layer="91"/>
<junction x="233.68" y="182.88"/>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="U$16" gate="G$1" pin="COM"/>
<pinref part="C2" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="360.68" y1="60.96" x2="360.68" y2="58.42" width="0.1524" layer="91"/>
<wire x1="360.68" y1="58.42" x2="365.76" y2="58.42" width="0.1524" layer="91"/>
<wire x1="370.84" y1="60.96" x2="370.84" y2="58.42" width="0.1524" layer="91"/>
<wire x1="370.84" y1="58.42" x2="365.76" y2="58.42" width="0.1524" layer="91"/>
<junction x="365.76" y="58.42"/>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="U$17" gate="G$1" pin="COM"/>
<pinref part="C9" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="284.48" y1="48.26" x2="287.02" y2="48.26" width="0.1524" layer="91"/>
<wire x1="287.02" y1="48.26" x2="287.02" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="COM"/>
<pinref part="J4" gate="G$1" pin="6"/>
</segment>
<segment>
<wire x1="320.04" y1="205.74" x2="317.5" y2="205.74" width="0.1524" layer="91"/>
<wire x1="317.5" y1="205.74" x2="317.5" y2="203.2" width="0.1524" layer="91"/>
<wire x1="317.5" y1="203.2" x2="317.5" y2="200.66" width="0.1524" layer="91"/>
<wire x1="317.5" y1="200.66" x2="317.5" y2="198.12" width="0.1524" layer="91"/>
<wire x1="317.5" y1="198.12" x2="317.5" y2="193.04" width="0.1524" layer="91"/>
<wire x1="320.04" y1="203.2" x2="317.5" y2="203.2" width="0.1524" layer="91"/>
<wire x1="320.04" y1="200.66" x2="317.5" y2="200.66" width="0.1524" layer="91"/>
<wire x1="320.04" y1="198.12" x2="317.5" y2="198.12" width="0.1524" layer="91"/>
<junction x="317.5" y="203.2"/>
<junction x="317.5" y="200.66"/>
<junction x="317.5" y="198.12"/>
<pinref part="U$24" gate="G$1" pin="COM"/>
<pinref part="U2" gate="G$1" pin="AGND"/>
<pinref part="U2" gate="G$1" pin="GND1"/>
<pinref part="U2" gate="G$1" pin="GND2"/>
<pinref part="U2" gate="G$1" pin="GND3"/>
</segment>
<segment>
<wire x1="363.22" y1="185.42" x2="363.22" y2="182.88" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="U$27" gate="G$1" pin="COM"/>
</segment>
<segment>
<wire x1="106.68" y1="154.94" x2="99.06" y2="154.94" width="0.1524" layer="91"/>
<wire x1="99.06" y1="154.94" x2="99.06" y2="142.24" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="1"/>
<pinref part="U$28" gate="G$1" pin="COM"/>
</segment>
<segment>
<wire x1="388.62" y1="193.04" x2="386.08" y2="193.04" width="0.1524" layer="91"/>
<wire x1="386.08" y1="193.04" x2="386.08" y2="190.5" width="0.1524" layer="91"/>
<pinref part="U$29" gate="G$1" pin="COM"/>
<pinref part="J3" gate="G$1" pin="GND"/>
</segment>
<segment>
<wire x1="416.56" y1="200.66" x2="416.56" y2="198.12" width="0.1524" layer="91"/>
<wire x1="416.56" y1="198.12" x2="416.56" y2="195.58" width="0.1524" layer="91"/>
<wire x1="416.56" y1="195.58" x2="416.56" y2="193.04" width="0.1524" layer="91"/>
<wire x1="416.56" y1="193.04" x2="416.56" y2="190.5" width="0.1524" layer="91"/>
<wire x1="414.02" y1="198.12" x2="416.56" y2="198.12" width="0.1524" layer="91"/>
<wire x1="414.02" y1="193.04" x2="416.56" y2="193.04" width="0.1524" layer="91"/>
<wire x1="414.02" y1="195.58" x2="416.56" y2="195.58" width="0.1524" layer="91"/>
<wire x1="414.02" y1="200.66" x2="416.56" y2="200.66" width="0.1524" layer="91"/>
<pinref part="U$30" gate="G$1" pin="COM"/>
<pinref part="J3" gate="G$1" pin="SH2"/>
<pinref part="J3" gate="G$1" pin="SH4"/>
<pinref part="J3" gate="G$1" pin="SH3"/>
<pinref part="J3" gate="G$1" pin="SH1"/>
</segment>
<segment>
<wire x1="53.34" y1="40.64" x2="53.34" y2="38.1" width="0.1524" layer="91"/>
<wire x1="58.42" y1="40.64" x2="58.42" y2="38.1" width="0.1524" layer="91"/>
<wire x1="58.42" y1="38.1" x2="55.88" y2="38.1" width="0.1524" layer="91"/>
<wire x1="55.88" y1="38.1" x2="53.34" y2="38.1" width="0.1524" layer="91"/>
<wire x1="55.88" y1="35.56" x2="55.88" y2="38.1" width="0.1524" layer="91"/>
<junction x="55.88" y="38.1"/>
<pinref part="U3" gate="G$1" pin="GND1"/>
<pinref part="U3" gate="G$1" pin="GND2"/>
<pinref part="U$32" gate="G$1" pin="COM"/>
</segment>
<segment>
<wire x1="101.6" y1="35.56" x2="101.6" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U$33" gate="G$1" pin="COM"/>
<pinref part="LED4" gate="G$1" pin="CA"/>
</segment>
<segment>
<wire x1="231.14" y1="76.2" x2="231.14" y2="73.66" width="0.1524" layer="91"/>
<pinref part="LED3" gate="G$1" pin="CA"/>
<pinref part="U$34" gate="G$1" pin="COM"/>
</segment>
<segment>
<wire x1="127" y1="147.32" x2="124.46" y2="147.32" width="0.1524" layer="91"/>
<wire x1="124.46" y1="147.32" x2="124.46" y2="144.78" width="0.1524" layer="91"/>
<pinref part="Y1" gate="G$1" pin="2"/>
<pinref part="U$13" gate="G$1" pin="COM"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="NC"/>
<wire x1="66.04" y1="109.22" x2="63.5" y2="109.22" width="0.1524" layer="91"/>
<wire x1="63.5" y1="109.22" x2="63.5" y2="106.68" width="0.1524" layer="91"/>
<wire x1="63.5" y1="106.68" x2="63.5" y2="104.14" width="0.1524" layer="91"/>
<wire x1="63.5" y1="104.14" x2="63.5" y2="101.6" width="0.1524" layer="91"/>
<wire x1="63.5" y1="101.6" x2="63.5" y2="83.82" width="0.1524" layer="91"/>
<wire x1="63.5" y1="83.82" x2="81.28" y2="83.82" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="NC1"/>
<wire x1="81.28" y1="83.82" x2="101.6" y2="83.82" width="0.1524" layer="91"/>
<wire x1="66.04" y1="106.68" x2="63.5" y2="106.68" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="NC2"/>
<wire x1="66.04" y1="104.14" x2="63.5" y2="104.14" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="NC3"/>
<wire x1="66.04" y1="101.6" x2="63.5" y2="101.6" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GND"/>
<wire x1="96.52" y1="111.76" x2="101.6" y2="111.76" width="0.1524" layer="91"/>
<wire x1="101.6" y1="111.76" x2="101.6" y2="109.22" width="0.1524" layer="91"/>
<wire x1="101.6" y1="109.22" x2="101.6" y2="106.68" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="NC7"/>
<wire x1="101.6" y1="106.68" x2="101.6" y2="104.14" width="0.1524" layer="91"/>
<wire x1="101.6" y1="104.14" x2="101.6" y2="101.6" width="0.1524" layer="91"/>
<wire x1="101.6" y1="101.6" x2="101.6" y2="83.82" width="0.1524" layer="91"/>
<wire x1="96.52" y1="109.22" x2="101.6" y2="109.22" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="NC6"/>
<wire x1="96.52" y1="106.68" x2="101.6" y2="106.68" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="NC5"/>
<wire x1="96.52" y1="104.14" x2="101.6" y2="104.14" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="NC4"/>
<wire x1="96.52" y1="101.6" x2="101.6" y2="101.6" width="0.1524" layer="91"/>
<wire x1="81.28" y1="83.82" x2="81.28" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="COM"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="K"/>
<wire x1="276.86" y1="147.32" x2="276.86" y2="129.54" width="0.1524" layer="91"/>
<pinref part="U$51" gate="G$1" pin="COM"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="VSS"/>
<wire x1="314.96" y1="147.32" x2="314.96" y2="127" width="0.1524" layer="91"/>
<pinref part="U$52" gate="G$1" pin="COM"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="RW"/>
<wire x1="304.8" y1="147.32" x2="304.8" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U$54" gate="G$1" pin="COM"/>
</segment>
<segment>
<pinref part="MENU" gate="G$1" pin="S"/>
<pinref part="U$57" gate="G$1" pin="COM"/>
</segment>
<segment>
<pinref part="SET" gate="G$1" pin="S"/>
<pinref part="U$58" gate="G$1" pin="COM"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<wire x1="86.36" y1="223.52" x2="109.22" y2="223.52" width="0.1524" layer="91"/>
<wire x1="109.22" y1="223.52" x2="109.22" y2="236.22" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="+5V"/>
<pinref part="J2" gate="G$1" pin="4"/>
</segment>
<segment>
<wire x1="71.12" y1="53.34" x2="76.2" y2="53.34" width="0.1524" layer="91"/>
<wire x1="76.2" y1="53.34" x2="76.2" y2="55.88" width="0.1524" layer="91"/>
<wire x1="81.28" y1="53.34" x2="76.2" y2="53.34" width="0.1524" layer="91"/>
<junction x="76.2" y="53.34"/>
<pinref part="U3" gate="G$1" pin="OUT"/>
<pinref part="U$18" gate="G$1" pin="+5V"/>
<pinref part="RP2" gate="B" pin="1"/>
</segment>
<segment>
<wire x1="210.82" y1="154.94" x2="210.82" y2="195.58" width="0.1524" layer="91"/>
<wire x1="205.74" y1="154.94" x2="210.82" y2="154.94" width="0.1524" layer="91"/>
<wire x1="210.82" y1="152.4" x2="210.82" y2="154.94" width="0.1524" layer="91"/>
<wire x1="205.74" y1="152.4" x2="210.82" y2="152.4" width="0.1524" layer="91"/>
<wire x1="210.82" y1="149.86" x2="210.82" y2="152.4" width="0.1524" layer="91"/>
<wire x1="205.74" y1="149.86" x2="210.82" y2="149.86" width="0.1524" layer="91"/>
<junction x="210.82" y="154.94"/>
<junction x="210.82" y="152.4"/>
<pinref part="U$9" gate="G$1" pin="+5V"/>
<pinref part="U1" gate="G$1" pin="VCC1"/>
<pinref part="U1" gate="G$1" pin="VCC2"/>
<pinref part="U1" gate="G$1" pin="AVCC"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="233.68" y1="195.58" x2="233.68" y2="198.12" width="0.1524" layer="91"/>
<wire x1="233.68" y1="198.12" x2="220.98" y2="198.12" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="220.98" y1="195.58" x2="220.98" y2="198.12" width="0.1524" layer="91"/>
<wire x1="220.98" y1="195.58" x2="210.82" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="180.34" y1="38.1" x2="177.8" y2="38.1" width="0.1524" layer="91"/>
<wire x1="177.8" y1="38.1" x2="177.8" y2="40.64" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="CA"/>
<pinref part="U$15" gate="G$1" pin="+5V"/>
</segment>
<segment>
<wire x1="284.48" y1="58.42" x2="287.02" y2="58.42" width="0.1524" layer="91"/>
<wire x1="287.02" y1="58.42" x2="287.02" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="+5V"/>
<pinref part="J4" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="142.24" y1="172.72" x2="142.24" y2="175.26" width="0.1524" layer="91"/>
<pinref part="RP1" gate="D" pin="2"/>
<pinref part="U$31" gate="G$1" pin="+5V"/>
</segment>
<segment>
<wire x1="355.6" y1="238.76" x2="360.68" y2="238.76" width="0.1524" layer="91"/>
<wire x1="360.68" y1="238.76" x2="360.68" y2="248.92" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VCC"/>
<pinref part="U$14" gate="G$1" pin="+5V"/>
</segment>
<segment>
<wire x1="378.46" y1="259.08" x2="378.46" y2="261.62" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="AN"/>
<pinref part="U$25" gate="G$1" pin="+5V"/>
</segment>
<segment>
<wire x1="391.16" y1="259.08" x2="391.16" y2="261.62" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="AN"/>
<pinref part="U$35" gate="G$1" pin="+5V"/>
</segment>
<segment>
<wire x1="320.04" y1="231.14" x2="312.42" y2="231.14" width="0.1524" layer="91"/>
<wire x1="312.42" y1="231.14" x2="312.42" y2="248.92" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VCCIO"/>
<pinref part="U$11" gate="G$1" pin="+5V"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VCC"/>
<wire x1="66.04" y1="116.84" x2="58.42" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U$36" gate="G$1" pin="+5V"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="VDD"/>
<wire x1="312.42" y1="147.32" x2="312.42" y2="119.38" width="0.1524" layer="91"/>
<wire x1="312.42" y1="119.38" x2="317.5" y2="119.38" width="0.1524" layer="91"/>
<pinref part="U$53" gate="G$1" pin="+5V"/>
<junction x="317.5" y="119.38"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="U$55" gate="G$1" pin="+5V"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="106.68" y1="129.54" x2="106.68" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="106.68" y1="132.08" x2="114.3" y2="132.08" width="0.1524" layer="91"/>
<wire x1="114.3" y1="132.08" x2="114.3" y2="134.62" width="0.1524" layer="91"/>
<pinref part="U$56" gate="G$1" pin="+5V"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<wire x1="101.6" y1="231.14" x2="86.36" y2="231.14" width="0.1524" layer="91"/>
<wire x1="101.6" y1="231.14" x2="101.6" y2="236.22" width="0.1524" layer="91"/>
<pinref part="U$7" gate="G$1" pin="VIN"/>
<pinref part="J2" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="35.56" y1="55.88" x2="35.56" y2="53.34" width="0.1524" layer="91"/>
<wire x1="35.56" y1="53.34" x2="40.64" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U$19" gate="G$1" pin="VIN"/>
<pinref part="U3" gate="G$1" pin="IN"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<wire x1="86.36" y1="198.12" x2="116.84" y2="198.12" width="0.1524" layer="91"/>
<wire x1="116.84" y1="198.12" x2="116.84" y2="236.22" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="3V3"/>
<pinref part="J2" gate="G$1" pin="14"/>
</segment>
<segment>
<wire x1="355.6" y1="205.74" x2="363.22" y2="205.74" width="0.1524" layer="91"/>
<wire x1="363.22" y1="205.74" x2="363.22" y2="208.28" width="0.1524" layer="91"/>
<wire x1="363.22" y1="195.58" x2="363.22" y2="205.74" width="0.1524" layer="91"/>
<junction x="363.22" y="205.74"/>
<pinref part="U$26" gate="G$1" pin="3V3"/>
<pinref part="C7" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="3V3OUT"/>
</segment>
</net>
<net name="D0/RX" class="0">
<segment>
<wire x1="238.76" y1="170.18" x2="205.74" y2="144.78" width="0.1524" layer="91"/>
<label x="210.82" y="144.78" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PD0(RXD)"/>
<pinref part="RP1" gate="B" pin="1"/>
<wire x1="238.76" y1="170.18" x2="238.76" y2="172.72" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="25.4" y1="228.6" x2="40.64" y2="228.6" width="0.1524" layer="91"/>
<label x="27.94" y="228.6" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="VUSB" class="0">
<segment>
<wire x1="195.58" y1="38.1" x2="198.12" y2="38.1" width="0.1524" layer="91"/>
<wire x1="198.12" y1="38.1" x2="198.12" y2="40.64" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="AN"/>
<pinref part="U$21" gate="G$1" pin="VUSB"/>
</segment>
<segment>
<wire x1="360.68" y1="71.12" x2="360.68" y2="73.66" width="0.1524" layer="91"/>
<wire x1="360.68" y1="73.66" x2="365.76" y2="73.66" width="0.1524" layer="91"/>
<wire x1="365.76" y1="73.66" x2="370.84" y2="73.66" width="0.1524" layer="91"/>
<wire x1="370.84" y1="73.66" x2="370.84" y2="71.12" width="0.1524" layer="91"/>
<wire x1="365.76" y1="76.2" x2="365.76" y2="73.66" width="0.1524" layer="91"/>
<junction x="365.76" y="73.66"/>
<pinref part="C8" gate="G$1" pin="1"/>
<pinref part="C9" gate="G$1" pin="1"/>
<pinref part="U$23" gate="G$1" pin="VUSB"/>
</segment>
<segment>
<wire x1="388.62" y1="203.2" x2="386.08" y2="203.2" width="0.1524" layer="91"/>
<wire x1="386.08" y1="203.2" x2="386.08" y2="208.28" width="0.1524" layer="91"/>
<pinref part="U$22" gate="G$1" pin="VUSB"/>
<pinref part="J3" gate="G$1" pin="VBUS"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="304.8" y1="236.22" x2="320.04" y2="236.22" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="DTR#"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<wire x1="304.8" y1="238.76" x2="320.04" y2="238.76" width="0.1524" layer="91"/>
<label x="307.34" y="238.76" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="TXD"/>
</segment>
<segment>
<wire x1="254" y1="172.72" x2="261.62" y2="172.72" width="0.1524" layer="91"/>
<label x="256.54" y="172.72" size="1.778" layer="95"/>
<pinref part="RP1" gate="B" pin="2"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<wire x1="304.8" y1="228.6" x2="320.04" y2="228.6" width="0.1524" layer="91"/>
<label x="307.34" y="228.6" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="RXD"/>
</segment>
<segment>
<wire x1="254" y1="165.1" x2="261.62" y2="165.1" width="0.1524" layer="91"/>
<label x="256.54" y="165.1" size="1.778" layer="95"/>
<pinref part="RP1" gate="C" pin="2"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="355.6" y1="198.12" x2="388.62" y2="198.12" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="USBDP"/>
<pinref part="J3" gate="G$1" pin="D+"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="355.6" y1="200.66" x2="388.62" y2="200.66" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="USBDM"/>
<pinref part="J3" gate="G$1" pin="D-"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="101.6" y1="53.34" x2="101.6" y2="50.8" width="0.1524" layer="91"/>
<wire x1="96.52" y1="53.34" x2="101.6" y2="53.34" width="0.1524" layer="91"/>
<pinref part="LED4" gate="G$1" pin="AN"/>
<pinref part="RP2" gate="B" pin="2"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="355.6" y1="226.06" x2="378.46" y2="226.06" width="0.1524" layer="91"/>
<wire x1="378.46" y1="226.06" x2="378.46" y2="228.6" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="CBUS0"/>
<pinref part="RP2" gate="C" pin="1"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="355.6" y1="223.52" x2="391.16" y2="223.52" width="0.1524" layer="91"/>
<wire x1="391.16" y1="223.52" x2="391.16" y2="228.6" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="CBUS1"/>
<pinref part="RP2" gate="D" pin="1"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="320.04" y1="215.9" x2="317.5" y2="215.9" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="CTS#"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<wire x1="320.04" y1="226.06" x2="317.5" y2="226.06" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="RI#"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="317.5" y1="223.52" x2="320.04" y2="223.52" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="NC1"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="317.5" y1="220.98" x2="320.04" y2="220.98" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="DSR#"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="317.5" y1="218.44" x2="320.04" y2="218.44" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="DCD#"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="355.6" y1="233.68" x2="358.14" y2="233.68" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="OSCO"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<wire x1="358.14" y1="231.14" x2="355.6" y2="231.14" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="OSCI"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<wire x1="358.14" y1="228.6" x2="355.6" y2="228.6" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="NC2"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<wire x1="358.14" y1="220.98" x2="355.6" y2="220.98" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="CBUS2"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<wire x1="358.14" y1="218.44" x2="355.6" y2="218.44" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="CBUS3"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<wire x1="358.14" y1="215.9" x2="355.6" y2="215.9" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="CBUS4"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<wire x1="358.14" y1="210.82" x2="355.6" y2="210.82" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="RESET#"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<wire x1="317.5" y1="210.82" x2="320.04" y2="210.82" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="TEST"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<wire x1="320.04" y1="233.68" x2="317.5" y2="233.68" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="RTS#"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="162.56" y1="149.86" x2="134.62" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PB6(XTAL1)"/>
<pinref part="Y1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="162.56" y1="144.78" x2="134.62" y2="144.78" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PB7(XTAL2)"/>
<pinref part="Y1" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<wire x1="251.46" y1="20.32" x2="248.92" y2="20.32" width="0.1524" layer="91"/>
<pinref part="RP1" gate="A" pin="1"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<wire x1="266.7" y1="20.32" x2="269.24" y2="20.32" width="0.1524" layer="91"/>
<pinref part="RP1" gate="A" pin="2"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="231.14" y1="91.44" x2="231.14" y2="88.9" width="0.1524" layer="91"/>
<pinref part="RP2" gate="A" pin="2"/>
<pinref part="LED3" gate="G$1" pin="AN"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="378.46" y1="243.84" x2="378.46" y2="246.38" width="0.1524" layer="91"/>
<pinref part="RP2" gate="C" pin="2"/>
<pinref part="LED1" gate="G$1" pin="CA"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="391.16" y1="243.84" x2="391.16" y2="246.38" width="0.1524" layer="91"/>
<pinref part="RP2" gate="D" pin="2"/>
<pinref part="LED2" gate="G$1" pin="CA"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VBAT"/>
<wire x1="96.52" y1="114.3" x2="119.38" y2="114.3" width="0.1524" layer="91"/>
<wire x1="119.38" y1="114.3" x2="119.38" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U$37" gate="G$1" pin="-"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="VO"/>
<wire x1="309.88" y1="147.32" x2="309.88" y2="96.52" width="0.1524" layer="91"/>
<pinref part="TM1" gate="G$1" pin="S"/>
<wire x1="309.88" y1="96.52" x2="307.34" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="A"/>
<wire x1="279.4" y1="147.32" x2="279.4" y2="106.68" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
